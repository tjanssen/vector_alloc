#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define MAX(a,b) ((a) > (b) ? (a) : (b))

#ifndef BLOCKSIZE
#define BLOCKSIZE 1
#endif

// this needs to be 64 bits since some shifting will happen
void morton_fill_lookup(uint64_t *lookup_table_x, uint64_t *lookup_table_y) {
	for (uint64_t i = 0; i < MAX(M_MAX, N_MAX); i++) {
		uint64_t inp = i;
		uint64_t result = inp & ((0b10<<BLOCKSIZE)-1);
		inp >>= 2*BLOCKSIZE;
		uint64_t position = 2*BLOCKSIZE;
		while (inp > 0) {
			result |= (inp & 0b1) << position;
			position += 2;
			inp >>= 1;
		}
		lookup_table_x[i] = result;
	}

	for (uint64_t i = 0; i < MAX(N_MAX, P_MAX); i++) {
		uint64_t inp = i;
		uint64_t result = (inp & ((0b10<<BLOCKSIZE)-1)) << BLOCKSIZE;
		inp >>= 2*BLOCKSIZE;
		uint64_t position = 2*BLOCKSIZE+1;
		while (inp > 0) {
			result |= (inp & 0b1) << position;
			position += 2;
			inp >>= 1;
		}
		lookup_table_y[i] = result;
	}
}
