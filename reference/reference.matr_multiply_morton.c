#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../benchmark/benchmark-timing.h"
#include "../reference/reference_helpers.h"

#define MAX(a,b) ((a) > (b) ? (a) : (b))

#ifndef M_MAX
	#define M_MAX 1024
#endif
#ifndef N_MAX
	#define N_MAX 1024
#endif
#ifndef P_MAX
	#define P_MAX 1024
#endif

// BLOCKSIZE defines n, where the block size is 2**n
#define BLOCKSIZE 1

double in_m0[M_MAX*N_MAX];
double in_m1[N_MAX*P_MAX];
double out_m_result[M_MAX*P_MAX];

double in_m1_t[P_MAX*N_MAX];

uint64_t lookup_table_x[MAX(M_MAX, N_MAX)];
uint64_t lookup_table_y[MAX(N_MAX, P_MAX)];

uint64_t morton_encode_LUT(uint64_t x, uint64_t y) {
	uint64_t result;
	result = lookup_table_x[x] | lookup_table_y[y];
	return result;
}

void block_multiply(double *m0, double *m1, double *m_result, uint64_t size, uint64_t cutoff) {
	if (size <= cutoff) {
		for (int m = 0; m<size; m++) {
			for (int p = 0; p<size; p++) {
				double accumulator = 0.0;
				for (int n = 0; n<size; n+=4) {
					uint64_t m0_elt = morton_encode_LUT(n,m);
					uint64_t m1_elt = morton_encode_LUT(n,p);
					for (int na=0; na<4; na++) {
						accumulator += m0[lookup_table_x[n+na] | lookup_table_y[m]] * 
													m1[lookup_table_x[n+na] | lookup_table_y[p]];
					}
					m0_elt++;
					m1_elt++;
				}
				m_result[morton_encode_LUT(p,m)] += accumulator;
			}
		}
	}
	else {
		size = size / 2;
		block_multiply(m0, m1, m_result, size, cutoff);
		block_multiply(m0+size*size, m1+2*size*size, m_result, size, cutoff);

		block_multiply(m0, m1+size*size, m_result+size*size, size, cutoff);
		block_multiply(m0+size*size, m1+3*size*size, m_result+size*size, size, cutoff);

		block_multiply(m0+2*size*size, m1+size*size, m_result+2*size*size, size, cutoff);
		block_multiply(m0+3*size*size, m1+2*size*size, m_result+2*size*size, size, cutoff);

		block_multiply(m0+2*size*size, m1+size*size, m_result+3*size*size, size, cutoff);
		block_multiply(m0+3*size*size, m1+3*size*size, m_result+3*size*size, size, cutoff);
	}
}

int main(int argc, char *argv[]) {
	struct timespec t0, t1;
	do_something_with_matrixarray(in_m0);
	do_something_with_matrixarray(in_m1);
	morton_fill_lookup(lookup_table_x, lookup_table_y);
	int i=0;
	while (i<64) {
		in_m0[i] = i+0.0000001; // C handles doubles in a stupid way
		in_m1[i] = i+16.0000001;
		i++;
	}
	clock_gettime(CLOCK_REALTIME, &t0);
	
	// transpose m1 (trust me bro)
	for (int y=0; y<P_MAX; y++) {
		for (int x=0; x<N_MAX; x++) {
			in_m1_t[morton_encode_LUT(y, x)] = in_m1[morton_encode_LUT(x, y)];
		}
	}
	
	block_multiply(in_m0, in_m1_t, out_m_result, M_MAX, 512);
	clock_gettime(CLOCK_REALTIME, &t1);
	printf("%f\n", get_gflops(M_MAX*P_MAX*(2*N_MAX-1), t0, t1));
	
	// this prints the resulting matrix for when you're doubting this program
	// for (int y=0; y<P_MAX; y++) {
	// 	for (int x=0; x<M_MAX; x++) {
	// 		printf("%.0f ", out_m_result[morton_encode_LUT(x, y)]);
	// 	}
	// 	printf("\n");
	// }
	
	do_something_with_matrixarray((double *)out_m_result);

	return 0;
}
