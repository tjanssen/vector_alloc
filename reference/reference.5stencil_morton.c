#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../benchmark/benchmark-timing.h"

#define MAX(a,b) ((a) > (b) ? (a) : (b))

#define OPS_PER_VALUE 5

#ifndef M_MAX
	#define M_MAX 1024
#endif
#ifndef N_MAX
	#define N_MAX 1024
#endif
#ifndef P_MAX
	#define P_MAX 1024
#endif

double m_in[M_MAX*N_MAX];
double m_out[M_MAX*N_MAX];

uint64_t lookup_table_x[MAX(M_MAX, N_MAX)];
uint64_t lookup_table_y[MAX(N_MAX, P_MAX)];

// this needs to be 64 bits since some shifting will happen
void fill_lookup() {
	for (uint64_t i = 0; i < MAX(M_MAX, N_MAX); i++) {
		uint64_t position = 0;
		uint64_t result = 0;
		uint64_t inp = i;
		while (inp > 0) {
			result |= (inp & 1) << position;
			position += 2;
			inp >>= 1;
		}
		lookup_table_x[i] = result;
	}

	for (uint64_t i = 0; i < MAX(N_MAX, P_MAX); i++) {
		uint64_t position = 1;
		uint64_t result = 0;
		uint64_t inp = i;
		while (inp > 0) {
			result |= (inp & 1) << position;
			position += 2;
			inp >>= 1;
		}
		lookup_table_y[i] = result;
	}
}

uint64_t morton_encode_LUT(uint64_t x, uint64_t y) {
	uint64_t result;
	result = lookup_table_x[x] | lookup_table_y[y];
	return result;
}

int main(int argc, char *argv[]) {
	struct timespec t0, t1;
	do_something_with_matrixarray(m_in);
	fill_lookup();
	int i=0;
	while (i<16) {
		m_in[i] = i+0.0000001; // C handles doubles in a stupid way
		m_in[i] = i+16.0000001;
		i++;
	}
	clock_gettime(CLOCK_REALTIME, &t0);
	for (int i=1; i<M_MAX-1; i++) {
		for (int j=1; j<N_MAX-1; j++) {
			double accumulator = 0.0;
			accumulator += m_in[morton_encode_LUT(i-1, j)];
			accumulator += m_in[morton_encode_LUT(i+1, j)];
			accumulator += m_in[morton_encode_LUT(i, j-1)];
			accumulator += m_in[morton_encode_LUT(i, j+1)];
			accumulator += m_in[morton_encode_LUT(i, j)];

			m_out[morton_encode_LUT(i, j)] = accumulator;
		}
	}
	clock_gettime(CLOCK_REALTIME, &t1);
	printf("%f\n", get_gflops(OPS_PER_VALUE*(M_MAX-2)*(N_MAX-2), t0, t1));
	do_something_with_matrixarray((double *)m_out);

	return 0;
}
