#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "../benchmark/benchmark-timing.h"
#include <string.h>

#ifndef M_MAX
	#define M_MAX 1024
#endif
#ifndef N_MAX
	#define N_MAX 1024
#endif
#ifndef P_MAX
	#define P_MAX 1024
#endif

#define MASK_X 21
#define MASK_Y 21
#define PADDED_X (M_MAX+MASK_X-1)
#define PADDED_Y (N_MAX+MASK_Y-1)
#define EDGE_X ((MASK_X-1)/2)
#define EDGE_Y ((MASK_Y-1)/2)

float *input_matrix[N_MAX];
float *padded_matrix[PADDED_Y];
float *output_matrix[N_MAX];

float *mask[MASK_Y];

int ref_image_read(char *path, float **dest) {
	FILE *image = fopen(path, "r");
	if (image == NULL) {
		printf("Opening the file \"%s\" failed.\n", path);
		return 0;
	}
	char buf[64];
	fgets(buf, 64, image);
	if (!strcmp(buf, "P5")) {
		printf("Only the P5 image format is currently supported, continue at your own risk. \n");
	}
	// do {
	// 	buf[0] = fgetc(image);
	// } while (buf[0] != '\n');
	uint64_t image_size[2];

	// This stops gdb's complaining about uninitialized variables.
	image_size[0] = 0;
	image_size[1] = 0;

	uint64_t image_white;
	fscanf(image, "%ld %ld\n", &image_size[0], &image_size[1]);
	fscanf(image, "%ld\n", &image_white);
	// printf("x: %d, y: %d, white: %d\n", image_size[0], image_size[1], image_white);
	uint64_t image_bytesize = image_size[0]*image_size[1]*(image_white+1) >> 8;
	uint64_t index[2];
	for (index[0]=0; index[0]<image_size[0]; index[0]++) {
		for (index[1]=0; index[1]<image_size[1]; index[1]++) {
			int c = fgetc(image);
			dest[index[0]][index[1]] = c*0.99999999;
		}
	}
	fclose(image);
	return 0;
}
void ref_image_write(float **src, char *path, uint64_t size_x, uint64_t size_y) {
	FILE *image_file = fopen(path, "w");
	if (image_file == NULL) {
		printf("Opening the file \"%s\" failed.\n", path);
		return;
	}
	// headers
	fputs("P5\n", image_file);
	fputs("# Created by vector_alloc\n", image_file);

	fprintf(image_file, "%ld %ld\n", size_x, size_y);
	// Change this if you want to use anything other than 8-bit color depth
	fputs("255\n", image_file);
	uint64_t image_index[2];
	for (image_index[0] = 0; image_index[0] < size_x; image_index[0]++) {
		for (image_index[1] = 0; image_index[1] < size_y; image_index[1]++) {
			char huts = src[image_index[0]][image_index[1]];
			fputc(huts, image_file);
		}
	}
	fclose(image_file);
}

int main() {
	struct timespec t0, t1;

	for (uint64_t y=0; y<N_MAX; y++) {
		input_matrix[y] = (float *)malloc(M_MAX*sizeof(float));
		output_matrix[y] = (float *)malloc(M_MAX*sizeof(float));
	}
	for (uint64_t y=0; y<PADDED_Y; y++) {
		padded_matrix[y] = (float *)malloc(PADDED_X*sizeof(float));
	}
	for (uint64_t y=0; y<MASK_Y; y++) {
		mask[y] = (float *)malloc(MASK_X*sizeof(float));
	}

	// replace this with a gaussian blur instead of an ugly box blur
	for (int y=0; y<MASK_Y; y++) {
		for (int x=0; x<MASK_X; x++) {
			mask[x][y] = 1.0;
		}
	}
	float mask_scale_factor = 1.0/441.00001;

	char in_path[64];
	char out_path[64];
	sprintf(in_path, "reference/extras/%dx%d.pgm", M_MAX, N_MAX);
	sprintf(out_path, "../%dx%dmask.pgm", M_MAX, N_MAX);

	ref_image_read(in_path, input_matrix);

	clock_gettime(CLOCK_REALTIME, &t0);

	// copy matrix from og to padded matrix
	for (uint64_t in_y = 0, pad_y = EDGE_Y; in_y < N_MAX; in_y++, pad_y++) {
		for (uint64_t in_x = 0, pad_x = EDGE_X; in_x < M_MAX; in_x++, pad_x++) {
			padded_matrix[pad_x][pad_y] = input_matrix[in_x][in_y];
		}
	}

	// mirror edges
	for (uint64_t in_y = EDGE_Y, padded_y = EDGE_Y; in_y < PADDED_Y - EDGE_Y; in_y++, padded_y++) {
		// left
		for (uint64_t in_x = EDGE_X+1, padded_x = EDGE_X-1; padded_x < MASK_X; in_x++, padded_x--) {
			padded_matrix[padded_x][padded_y] = padded_matrix[in_x][in_y];
		}

		// right
		for (uint64_t in_x = EDGE_X+M_MAX-2, padded_x = EDGE_X+M_MAX; in_x >= M_MAX-1; in_x--, padded_x++) {
			padded_matrix[padded_x][padded_y] = padded_matrix[in_x][in_y];
		}
	}

	for (uint64_t in_x = 0, padded_x = 0; in_x < PADDED_X; in_x++, padded_x++) {
		// top
		for (uint64_t in_y = EDGE_Y+1, padded_y = EDGE_Y-1; padded_y < MASK_Y; in_y++, padded_y--) {
			padded_matrix[padded_x][padded_y] = padded_matrix[in_x][in_y];
		}

		// bottom
		for (uint64_t in_y = EDGE_Y+N_MAX-2, padded_y = EDGE_Y+N_MAX; in_y >= N_MAX-1; in_y--, padded_y++) {
			padded_matrix[padded_x][padded_y] = padded_matrix[in_x][in_y];
		}
	}

	for (uint64_t out_y = 0; out_y < N_MAX; out_y++) {
		for (uint64_t out_x = 0; out_x < M_MAX; out_x++) {
			float cell_result = 0.0;
			for (uint64_t mask_y = 0, in_y = out_y; mask_y < MASK_Y; mask_y++, in_y++) {
				for (uint64_t mask_x = 0, in_x = out_x; mask_x < MASK_X; mask_x++, in_x++) {
					cell_result += (padded_matrix[in_y][in_x] * mask[mask_y][mask_x])*mask_scale_factor;
				}
			}
			output_matrix[out_x][out_y] = cell_result;
		}
	}

	clock_gettime(CLOCK_REALTIME, &t1);

	ref_image_write(padded_matrix, "../huts.pgm", PADDED_X, PADDED_Y);
	ref_image_write(output_matrix, out_path, M_MAX, N_MAX);
	printf("%f\n", get_gflops((uint64_t)M_MAX*N_MAX*(2*MASK_X*MASK_Y-1), t0, t1));

	for (uint64_t y=0; y<N_MAX; y++) {
		free(input_matrix[y]);
		free(output_matrix[y]);
	}
	for (uint64_t y=0; y<PADDED_Y; y++) {
		free(padded_matrix[y]);
	}
	for (uint64_t y=0; y<MASK_Y; y++) {
		free(mask[y]);
	}

	return 0;
}
