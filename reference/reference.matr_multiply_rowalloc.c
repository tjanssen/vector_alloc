#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../benchmark/benchmark-timing.h"

#ifndef M_MAX
	#define M_MAX 1024
#endif
#ifndef N_MAX
	#define N_MAX 1024
#endif
#ifndef P_MAX
	#define P_MAX 1024
#endif

double *m0[M_MAX];
double *m1[N_MAX];
double *m1_t[P_MAX];
double *m_result[M_MAX];

int main(int argc, char *argv[]) {
	struct timespec t0, t1;
	for (uint64_t m=0; m<M_MAX; m++) {
		m0[m] = malloc(N_MAX*sizeof(double));
		m_result[m] = malloc(P_MAX*sizeof(double));
	}
	for (uint64_t n=0; n<N_MAX; n++) {
		m1[n] = malloc(P_MAX*sizeof(double));
	}
	do_something_with_matrixarray((double *)m0);
	do_something_with_matrixarray((double *)m1);
	clock_gettime(CLOCK_REALTIME, &t0);
	// transpose m1
	for (int p = 0; p<P_MAX; p++) {
		m1_t[p] = malloc(N_MAX*sizeof(double));
		for (int n = 0; n<N_MAX; n++) {
			m1_t[p][n] = m1[n][p];
		}
	}
	for (int m = 0; m<M_MAX; m++) {
		for (int p = 0; p<P_MAX; p++) {
			double accumulator = 0.0;
			for (int n = 0; n<N_MAX; n++) {
				accumulator += m0[m][n] * m1_t[p][n];
			}
			m_result[m][p] = accumulator;
		}
	}
	clock_gettime(CLOCK_REALTIME, &t1);
	printf("%f\n", get_gflops(M_MAX*P_MAX*(2*N_MAX-1), t0, t1));
	do_something_with_matrixarray((double *)m_result);

	for (uint64_t m=0; m<M_MAX; m++) {
		free(m0[m]);
		free(m_result[m]);
	}
	for (uint64_t n=0; n<N_MAX; n++) {
		free(m1[n]);
	}
	for (uint64_t p=0; p<P_MAX; p++) {
		free(m1_t[p]);
	}
	return 0;
}
