#include "../benchmark/benchmark-timing.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define OPS_PER_VALUE 5

#ifndef M_MAX
	#define M_MAX 1024
#endif
#ifndef N_MAX
	#define N_MAX 1024
#endif
#ifndef P_MAX
	#define P_MAX 1024
#endif

double *m_in[M_MAX];
double *m_out[M_MAX];

// The third argument is ignored. Blame the Makefile.
int main(int argc, char *argv[]) {
	struct timespec t0, t1;
	for (int i=0; i<N_MAX; i++) {
		m_in[i] = (double *)malloc(sizeof(double)*N_MAX);
		m_out[i] = (double *)malloc(sizeof(double)*N_MAX);
	}
	do_something_with_matrixarray((double *)m_in);
	clock_gettime(CLOCK_REALTIME, &t0);
	for (int i=1; i<M_MAX-1; i++) {
		for (int j=1; j<N_MAX-1; j++) {
			double accumulator = 0.0;
            accumulator += m_in[i-1][j];
            accumulator += m_in[i+1][j];
            accumulator += m_in[i][j-1];
            accumulator += m_in[i][j+1];
            accumulator += m_in[i][j];

			m_out[i-1][j-1] = accumulator;
		}
	}

	clock_gettime(CLOCK_REALTIME, &t1);
	printf("%f\n", get_gflops(OPS_PER_VALUE*(M_MAX-2)*(N_MAX-2), t0, t1));
	for (int i=0; i<M_MAX; i++) {
		do_something_with_matrixarray((double *)m_out[i]);
	}
}
