#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../benchmark/benchmark-timing.h"

#ifndef M_MAX
	#define M_MAX 1024
#endif
#ifndef N_MAX
	#define N_MAX 1024
#endif
#ifndef P_MAX
	#define P_MAX 1024
#endif

double m0[M_MAX][N_MAX];
double m1[N_MAX][P_MAX];
double m1_t[P_MAX][N_MAX];
double m_result[M_MAX][P_MAX];

int main(int argc, char *argv[]) {

	// double *m0 = malloc(m_max*n_max*sizeof(double));
	// double *m1 = malloc(n_max*p_max*sizeof(double));
	// double *m_result = malloc(m_max*p_max*sizeof(double));

	struct timespec t0, t1;
	do_something_with_matrixarray((double *)m0);
	do_something_with_matrixarray((double *)m1);
	clock_gettime(CLOCK_REALTIME, &t0);
	// transpose m1
	for (int n = 0; n<N_MAX; n++) {
		for (int p = 0; p<P_MAX; p++) {
			m1_t[p][n] = m1[n][p];
		}
	}
	for (int m = 0; m<M_MAX; m++) {
		for (int p = 0; p<P_MAX; p++) {
			double accumulator = 0.0;
			for (int n = 0; n<N_MAX; n++) {
				accumulator += m0[m][n] * m1_t[p][n];
			}
			m_result[m][p] = accumulator;
		}
	}
	clock_gettime(CLOCK_REALTIME, &t1);
	printf("%f\n", get_gflops(M_MAX*P_MAX*(2*N_MAX-1), t0, t1));
	do_something_with_matrixarray((double *)m_result);

	// free(m0);
	// free(m1);
	// free(m_result);
	return 0;
}
