#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "../vect_t-core.h"

struct VECT_T {
  VECT_DATATYPE *data;
  uint32_t size;
  uint32_t total_size;
  uint16_t rc;
  vect_t* next;
  uint32_t next_offset;
  struct LAST_ACCESSED_ {
    vect_t *result;
    uint32_t global_index;
  } last_accessed;
};

void vect_free(vect_t *input);

void inc_rc_l(vect_t *source, int amount, uint32_t size) {
  bool free_source = false;
  if (source->rc <= 1 && amount < 0) {
    free(source->data);
    free_source = true;
  }
  else {
    source->rc += amount;
  }
  if (source->next != NULL/* && size > source->size*/) {
    inc_rc_l(source->next, amount, size - source->size);
  } 
  if (free_source) free(source);
}

void inc_rc(vect_t *source, int amount) {
  inc_rc_l(source, amount, source->total_size);
}

VECT_DATATYPE *vect_get_addr_nr(vect_t *source, uint32_t index) {
  uint32_t start_index = index;
  vect_t *start_vector = source;
  if (index >= source->last_accessed.global_index) {
    index = index - source->last_accessed.global_index;
    source = source->last_accessed.result;
  }
  while (index >= source->size) {
    if (source->next == NULL) {
      printf("Vector index out of range");
      return NULL;
    }
    // VECT_DATATYPE *result = vect_get_addr_nr(source->next, index-source->size+source->next_offset);
    index = index-source->size+source->next_offset;
    source->last_accessed.global_index += source->size-source->next_offset;
    source = source->next;
  }
  start_vector->last_accessed.result = source;
  start_vector->last_accessed.global_index = start_index-index;
  return source->data + index;
}

// this implementation actually counts the number of elements
// uint32_t vect_get_size_nr(vect_t *source) {
//   uint32_t size = 0;
//   if (source == NULL) return 0;
//   if (source->next != NULL) size += vect_get_size_nr(source->next)-source->next_offset;
//   return size+source->size;
// }

// this implementation keeps track of the number of elements
uint32_t vect_get_size_nr(vect_t *source) {
  return source->total_size;
}

vect_t *vect_alloc(uint32_t vec_size) {
  vect_t *newVector = malloc(sizeof(vect_t));
  newVector->data = malloc(sizeof(VECT_DATATYPE)*vec_size);
  newVector->size = vec_size;
  newVector->total_size = vec_size;
  newVector->rc = 1;
  newVector->next = NULL;
  newVector->next_offset = 0;
  newVector->last_accessed.result = newVector;
  newVector->last_accessed.global_index = 0;
  return newVector;
}

vect_t *vect_realloc(vect_t *input, uint32_t new_size) {
  input->total_size = new_size;
  if (input->size >= new_size) {
    if (input->rc <= 1) {
      input->data = realloc(input->data, new_size*sizeof(VECT_DATATYPE));
      input->size = new_size;
      if (input->next != NULL) inc_rc_l(input->next, -1, vect_get_size_nr(input->next)); // TODO remove reference counting D:<
      input->next = NULL;
    }
    else {
      input->rc--;
    }
  }
  else {
    input->rc--;
    vect_realloc(input->next, new_size - input->size + input->next_offset);
  }
  return input;
}

vect_t *vect_reference(vect_t *target, uint32_t offset) {
  vect_t *newVector = malloc(sizeof(vect_t));
  newVector->data = NULL;
  newVector->size = 0;
  newVector->total_size = target->total_size-offset;
  if (newVector->total_size > target->total_size) {
    printf("Selected offset is larger than target vector size.");
  }
  newVector->rc = 1;
  while (offset >= target->size) {
    offset -= target->size-target->next_offset;
    target = target->next;
  }
  newVector->next = target;
  newVector->next_offset = offset;
  inc_rc_l(target, 1, target->total_size);
  return newVector;
}

vect_t *vect_select_nr(vect_t *source, uint32_t offset, uint32_t total_size) {
  // checks are not necessary since vect_reference checks for out-of-bounds offset
  vect_t *newVector;
  if (source == NULL) newVector = vect_alloc(total_size);
  else if (source->rc == 1) {
    newVector = vect_reference(source, offset);
    newVector->total_size = total_size;
  }
  else {
    newVector = vect_alloc(total_size);
    for (int i=0; i<total_size; i++) {
      *vect_get_addr_nr(newVector, i) = *vect_get_addr_nr(source, i+offset);
    }
  }
  return newVector;
}

vect_t *vect_merge_nr(vect_t *source_1, vect_t *source_2) {
  /* 
   * this function assumes that intermediate segments are allowed to have 
   * incorrect total_size
   */
  if (source_1->rc == 1 && source_2->rc == 1) {
    vect_t *last_vect = source_1;
    while (last_vect->next != NULL) {
      last_vect->total_size += source_2->total_size;
      last_vect = last_vect->next;
    }
    last_vect->next = source_2;
    last_vect->total_size = source_1->total_size+source_2->total_size;
    
    /* this filthy workaround is needed because the reference counter wrapper 
     * decreases the rc for both sources */
    inc_rc(source_1, 1);
    inc_rc(source_2, 1);
    
    return source_1;
  }
  else {
    vect_t *new_vec = vect_select_nr(NULL, 0, source_1->total_size+source_2->total_size);
    for (int i=0; i<source_1->total_size; i++) {
      new_vec->data[i] = *vect_get_addr_nr(source_1, i);
    }
    for (int i=0; i<source_2->total_size; i++) {
      new_vec->data[i+source_1->total_size] = *vect_get_addr_nr(source_2, i);
    }
    return new_vec;
  }
}


