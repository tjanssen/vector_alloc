/*
 * This core will always allocate an array of size 10 and will truncate when 
 * the vector becomes too long. Please only use this for testing functions 
 * because it will cause issues otherwise. 
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "../vect_t-core.h"

// remove this one later maybe
#include <stdio.h>

struct DATA {
  uint16_t rc;
  VECT_DATATYPE elements;
};

struct VECT_T {
  struct DATA *data;
  uint32_t offset;
  uint32_t size;
  uint16_t rc;
};

VECT_DATATYPE vect_get_nr(vect_t *v, uint32_t index) {
  return *(&v->data->elements+index+v->offset);
}

void vect_set_nr(vect_t *v, uint32_t index, VECT_DATATYPE elt) {
  if (v->rc != v->data->rc) {
    struct DATA *old_data = v->data;
    v->data = malloc(sizeof(struct DATA)+sizeof(VECT_DATATYPE)*(v->size-1));
    v->data->rc = v->rc;
		v->offset = 0;
    memcpy(&v->data->elements, &old_data->elements+v->offset, v->size*sizeof(VECT_DATATYPE));
  }
  *(&v->data->elements+index+v->offset) = elt;
}

uint32_t vect_get_size_nr(vect_t *source) {
  return source->size;
}

vect_t *vect_alloc(uint32_t size) {
  vect_t *newVector = malloc(sizeof(vect_t));
  newVector->size = size;
  if (size > 0) {
    newVector->data = malloc(sizeof(struct DATA)+sizeof(VECT_DATATYPE)*(size-1));
  }
  else {
    newVector->data = malloc(sizeof(struct DATA));
  }
  newVector->rc = 1;
  newVector->data->rc = 1;
	newVector->offset = 0;
  return newVector;
}

vect_t *vect_select_nr(vect_t *source, uint32_t offset, uint32_t size) {
  // remove this if you trust the programmer (never)
  if (source->size < offset+size) printf("Warning: Selected size is larger than vector size.\n");
  vect_t *new_v = malloc(sizeof(vect_t));
  new_v->data = source->data;
  new_v->offset = offset+source->offset;
  new_v->size = size;
  new_v->rc = 1;
  new_v->data->rc++;
  return new_v;
}

vect_t *vect_insert(vect_t *source_1, vect_t *source_2, uint32_t offset) {
  vect_t *newVector = vect_alloc(source_1->size + source_2->size);
  memcpy(&newVector->data->elements, &source_2->data->elements, offset*sizeof(VECT_DATATYPE));
  memcpy(&newVector->data->elements+offset, &source_1->data->elements, source_1->size*sizeof(VECT_DATATYPE));
  memcpy(&newVector->data->elements+offset+source_1->size, &source_2->data->elements+offset, (source_2->size-offset)*sizeof(VECT_DATATYPE));
  dec_rc(source_1, 1);
  dec_rc(source_2, 1);
  
  return newVector;
}

void inc_rc(vect_t *source, uint16_t amount) {
  source->rc+=amount;
  source->data->rc+=amount;
}

void dec_rc(vect_t *source, uint16_t amount) {
  source->rc-=amount;
  source->data->rc-=amount;
  if (source->rc == 0) {
    if (source->data->rc == 0) {
      free(source->data);
    }
    free(source);
  }
}
