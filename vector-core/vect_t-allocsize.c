/*
 * This core will always allocate an array of size 10 and will truncate when 
 * the vector becomes too long. Please only use this for testing functions 
 * because it will cause issues otherwise. 
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "../vect_t-core.h"

// remove this one later maybe
#include <stdio.h>

// how many times bigger should the allocation be to the size
#define VECTOR_BUFFER 2

struct VECT_T {
  uint16_t rc;
	uint32_t size;
  uint32_t alloc_size;
  VECT_DATATYPE *data;
};

VECT_DATATYPE vect_get_nr(vect_t *v, uint32_t index) {
  return *(v->data+index);
}

void vect_set_nr(vect_t *v, uint32_t index, VECT_DATATYPE elt) {
  *(v->data+index) = elt;
}

uint32_t vect_get_size_nr(vect_t *source) {
  return source->size;
}

vect_t *vect_alloc(uint32_t size) {
  vect_t *newVector = malloc(sizeof(vect_t));
  newVector->rc = 1;
	newVector->size = size;
	if (size < 20) {
		newVector->alloc_size = 20*VECTOR_BUFFER;
	} 
	else {
		newVector->alloc_size = VECTOR_BUFFER*size;
	}
  newVector->data = malloc(sizeof(VECT_DATATYPE)*newVector->alloc_size);
  return newVector;
}

vect_t *vect_select_nr(vect_t *source, uint32_t offset, uint32_t size) {
  vect_t *newVector = vect_alloc(size);
  
  // remove this if you trust the programmer (never)
  if (source->size < offset+size) printf("Warning: Selected size is larger than vector size.\n");
  
  memcpy(newVector->data, source->data+offset, size*sizeof(VECT_DATATYPE));
  return newVector;
}

vect_t *safe_allocate(vect_t *source_1, vect_t *source_2, uint32_t offset) {
  vect_t *newVector = vect_alloc(source_1->size + source_2->size);
  memcpy(newVector->data, source_2->data, offset*sizeof(VECT_DATATYPE));
  memcpy(newVector->data+offset, source_1->data, source_1->size*sizeof(VECT_DATATYPE));
  memcpy(newVector->data+offset+source_1->size, source_2->data+offset, (source_2->size-offset)*sizeof(VECT_DATATYPE));
  dec_rc(source_1, 1);
  dec_rc(source_2, 1);
  
  return newVector;
}

vect_t *vect_insert(vect_t *source_1, vect_t *source_2, uint32_t offset) {
	if (source_1->size+source_2->size <= source_2->alloc_size) {
		if (source_2->rc <= 1 && offset+source_1->size >= source_2->size) {
			memcpy(source_2->data+source_1->size+offset, source_2->data+offset, (source_2->size-offset)*sizeof(VECT_DATATYPE));
			memcpy(source_2->data, source_1->data+offset, source_1->size*sizeof(VECT_DATATYPE));
			source_2->size+=source_1->size;
			dec_rc(source_1, 1);
			return source_2;
		}
		else if (source_1->rc <= 1 && source_1->size < offset) {
			memcpy(source_1->data+offset, source_1->data, source_1->size*sizeof(VECT_DATATYPE));
			memcpy(source_1->data, source_2->data, offset*sizeof(VECT_DATATYPE));
			memcpy(source_1->data+source_1->size+offset, source_2->data+offset, (source_2->size-offset)*sizeof(VECT_DATATYPE));
			source_1->size+=source_2->size;
			dec_rc(source_2, 1);
			return source_1;
		}
		else return safe_allocate(source_1, source_2, offset);
	}
	else return safe_allocate(source_1, source_2, offset);
}

void inc_rc(vect_t *source, uint16_t amount) {
  source->rc+=amount;
}

void dec_rc(vect_t *source, uint16_t amount) {
  source->rc-=amount ;
  if (source->rc == 0) {
    free(source->data);
    free(source);
  }
}
