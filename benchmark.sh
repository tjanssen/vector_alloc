# fuck bash I hope it dies
# c=0;
# for foo in $5; do
# 	c=`expr $c + 1`;
# done
# echo $c;

runtimes=();
matrixBenchmarks=();
noMatrixBenchmarks=();
for BENCHMARK in $1; do
	if [[ "$BENCHMARK" =~ .*\.matrix ]]; then
		matrixBenchmarks+=" $BENCHMARK";
	else
		noMatrixBenchmarks+=" $BENCHMARK";
	fi
done
for COREFILE in $2; do
	subRuntimes0=();
	echo -n "Benchmarking $COREFILE ...\\n";
	for BENCHMARK in $noMatrixBenchmarks; do
		output=$((`bin/bench-$COREFILE.$BENCHMARK`));			
		echo -n "  $BENCHMARK runtime: $output\\n";
		subRuntimes0+=( $output );
	done
	runtimes+=( $subRuntimes0 );
	for MATRIX_COREFILE in $3; do
		echo -n "  Benchmarking $MATRIX_COREFILE ...\\n";
		subRuntimes1=();
		for BENCHMARK in $matrixBenchmarks; do
			echo -n "    Doing benchmark $BENCHMARK $4 times ...\\n";
			subRuntimes2=();
			for SIZE in $5; do
				echo -n "      $SIZE performance in GFLOPS: ";
				subRuntimes3=();
				for PASSES in $(seq 1 $4); do
					output=`bin/bench-$MATRIX_COREFILE.$COREFILE.$BENCHMARK $(printf $SIZE | sed 's|[()]||g' | sed 's|,| |g')`;
					subRuntimes3+=( $output );
					echo -n "$output ";
				done
				echo -n "\\n";
				subRuntimes2+=( $subRuntimes3 );
			done
			subRuntimes1+=( $subRuntimes2 );
		done
		subRuntimes0+=( $subRuntimes1 );
	done
done
