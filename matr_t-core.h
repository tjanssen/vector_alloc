#include "vect_t-core.h"

typedef struct MATR_T matr_t;

/**
 * Increase a matrix's reference counter.
 * @param m Matrix of which the reference counter is to be increased.
 * @param amount Amount by which the reference counter is to be increased. 
 */
void matr_inc_rc(matr_t *m, uint16_t amount) ;

/**
 * Decrease a matrix's reference counter.
 * @param m Matrix of which the reference counter is to be decreased.
 * @param amount Amount by which the reference counter is to be decreased. 
 */
void matr_dec_rc(matr_t *m, uint16_t amount) ;

/**
 * Get an element from a matrix. 
 * This function does not touch the reference counter of any matrix. 
 * @param m Matrix from which the element should be retrieved. 
 * @param index Array that denotes the index of the element. index should have the same length as the dimensionality of m. 
 * @return The element. 
 */
VECT_DATATYPE matr_get_nr(matr_t *m, uint64_t index[]) ;

/**
 * Set an element in a matrix. 
 * This function does not touch the reference counter of any matrix. 
 * @param m Matrix in which the element should be set. 
 * @param index Array that denotes the index of the element. index should have the same length as the dimensionality of m. 
 * @param elt New value of the element. 
 */
void matr_set_nr(matr_t *m, uint64_t index[], VECT_DATATYPE elt) ;

/**
 * Get the dimensionality of a matrix. 
 * This function does not touch the reference counter of any matrix. 
 * @param m Matrix from which the dimensionality should be retrieved. 
 * @return Dimensionality of the matrix. 
 */
uint64_t matr_get_dim_nr(matr_t *m) ;

/**
 * Get the size of a matrix. 
 * This function does not touch the reference counter of any matrix. 
 * @param m Matrix from which the size should be retrieved. 
 * @return Array with length dimensionality of m that denotes the size of m. 
 */
uint64_t *matr_get_size_nr(matr_t *m) ;

/**
 * Allocate a matrix
 * @param dimensionality Dimensionality of the matrix. 
 * @param size Array containing the size of the new matrix. This array should have length dimensionality. 
 * @return The allocated matrix. 
 */
matr_t *matr_alloc(uint64_t dimensionality, uint64_t size[]) ;

/**
 * Select a submatrix. 
 * This function does not touch the reference counter of the matrix from which the submatrix is taken. 
 * @param source Matrix from which the submatrix should be selected. 
 * @param offset Array containing the offsets of the selection. This array should have length equal to the dimensionality of source, even if any of the elements is 0.  
 * @param size Array denoting the size of the submatrix. This array should have length equal to the dimensionality of source, even if a dimension has size 1. 
 * @return The selected submatrix. 
 */
matr_t *matr_select_nr(matr_t *source, uint64_t offset[], uint64_t size[]) ;
