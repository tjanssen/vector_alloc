for BENCHMARK in $1; do
	echo -n "Benchmarking reference $BENCHMARK ...\\n"
	for SIZE in $3; do
		echo -n "  $SIZE performance in GFLOPS: ";
		for PASSES in $(seq 1 $2); do
			output=`bin/$BENCHMARK.$SIZE $(printf $SIZE | sed 's|[()]||g' | sed 's|,| |g')`;
			echo -n "$output ";
		done
		echo -n "\\n";
	done
done
