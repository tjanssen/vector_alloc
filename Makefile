# CFLAGS
CFLAGS := -Ofast -ftree-vectorize -flto $(shell if [[ `grep avx /proc/cpuinfo | wc -m` -ne 0 ]]; then echo "-mavx"; else echo "-march=native"; fi) -Wall

# Vector core
CORE := vect_t-simple

# Matrix core
MATRIX_CORE := matr_t-simple-flat

# Matrix sizes
MATRIX_SIZE := 256,256,256 512,512,512 1024,1024,1024 2048,2048,2048 4096,4096,4096

# Tests
# TESTS := getset.int32_t.matrix matr_copy.int32_t.matrix matr_map.int32_t.matrix matr_select.int32_t.matrix matr_select_set.int32_t.matrix matr_multiply.int32_t.matrix matr_negate.int32_t.matrix matr_block.int32_t.matrix
TESTS := image_mask.uint8_t.matrix

# Benchmarks
BENCHMARKS := 21x21mask.float.matrix

# Reference benchmarks
BENCHMARK_REF := reference.21x21mask_morton

# Number of passes for benchmarks
PASSES := 12

# Directories
BIN_DIR := bin
CORE_DIR := vector-core
MATRIX_CORE_DIR := matrix-core
BUILD_DIR := build
UTILS_DIR := vector-utils
MATRIX_UTILS_DIR := matrix-utils
TEST_DIR := testing
BENCH_DIR := benchmark
REFERENCE_DIR := reference
TEST_BUILD_DIR := ${TEST_DIR}/build
BENCH_BUILD_DIR := ${BENCH_DIR}/build
CORE_BUILD_DIR := ${CORE_DIR}/build
MATRIX_CORE_BUILD_DIR := ${MATRIX_CORE_DIR}/build

# Test variables
# TESTS := getset.int32_t.matrix matr_vect_select.int32_t.matrix transpose.int32_t.matrix rc.int32_t getset.int32_t getset.double merge.int32_t merge-size.int32_t seq-read.int32_t multiple-read.int32_t selectempty.int32_t selectcopy.int32_t
TEST_OBJ := $(shell echo $(TESTS) | sed -e 's|\([^ ]*\)|${TEST_BUILD_DIR}/\0.o|g')

# Benchmark variables
BENCH_OBJ := $(foreach TYPE,$(shell echo ${BENCHMARKS} | sed -e 's|\.matrix||g' | sed -e 's|[^. ]*\.||g'),${BENCH_BUILD_DIR}/benchmark-utils.${TYPE}.o ${BENCH_BUILD_DIR}/benchmark-timing.${TYPE}.o) $(shell echo $(BENCHMARKS) | sed -e 's|\([^ ]*\)|${BENCH_BUILD_DIR}/\0.o|g')
BENCHMARK_REF_BIN := $(foreach B,${BENCHMARK_REF},$(foreach S,${MATRIX_SIZE},${BIN_DIR}/${B}.${S}))
BENCHMARK_REF_SRC := $(foreach S,${BENCHMARK_REF},${REFERENCE_DIR}/${S}.c)

# Core and Matrix core settings
TYPES := $(shell echo -n $(foreach T,${TESTS},$(lastword $(subst ., ,$(subst .matrix,,$T)))) $(foreach B,${BENCHMARKS},$(lastword $(subst ., ,$(subst .matrix,,$B)))) | sed -e 's| |\n|g' | sort -u)
CORE_SRC := $(foreach F,${CORE},${CORE_DIR}/$F.c)
CORE_TEST_OBJ := $(foreach C,${CORE_SRC},$(foreach T,${TESTS},${CORE_BUILD_DIR}/$(shell echo $C | sed -e 's|[^/]*/||g' | sed -e 's|\.c||').$(lastword $(subst ., ,$(subst .matrix,,$T)).o)))
CORE_BENCH_OBJ := $(foreach C,${CORE_SRC},$(foreach B,${BENCHMARKS},${CORE_BUILD_DIR}/$(shell echo $C | sed -e 's|[^/]*/||g' | sed -e 's|\.c||').$(lastword $(subst ., ,$(subst .matrix,,$B)).o)))
MATRIX_CORE_SRC := $(foreach F,${MATRIX_CORE},${MATRIX_CORE_DIR}/$F.c)
MATRIX_CORE_TEST_OBJ := $(shell echo $(foreach C,${MATRIX_CORE_SRC},$(foreach T,${TESTS},${MATRIX_CORE_BUILD_DIR}/$(shell echo $C | sed -e 's|[^/]*/||g' | sed -e 's|\.c||').$(lastword $(subst ., ,$(subst .matrix,,$T)).o))) | sed -e 's| |\n|g' | sort -u)
MATRIX_CORE_BENCH_OBJ := $(shell echo $(foreach C,${MATRIX_CORE_SRC},$(foreach B,${BENCHMARKS},${MATRIX_CORE_BUILD_DIR}/$(shell echo $C | sed -e 's|[^/]*/||g' | sed -e 's|\.c||').$(lastword $(subst ., ,$(subst .matrix,,$B)).o))) | sed -e 's| |\n|g' | sort -u)
MATRIX_UTILS_SRC := ${MATRIX_UTILS_DIR}/matr_t-rc.c ${MATRIX_UTILS_DIR}/matr_t-utils.c ${MATRIX_UTILS_DIR}/matr_t-transpose.c ${MATRIX_UTILS_DIR}/matr_t-multiplication.c ${MATRIX_UTILS_DIR}/matr_t-image.c
MATRIX_UTILS_OBJ := $(foreach S,${MATRIX_UTILS_SRC},$(foreach T,${TYPES},${BUILD_DIR}/$(subst ${MATRIX_UTILS_DIR}/,,$(subst .c,,${S}).$T.o)))
UTILS_SRC := ${UTILS_DIR}/vect_t-utils.c ${UTILS_DIR}/vect_t-sorting.c ${UTILS_DIR}/vect_t-rc.c
UTILS_OBJ := $(foreach S,${UTILS_SRC},$(foreach T,${TYPES},${BUILD_DIR}/$(subst ${UTILS_DIR}/,,$(subst .c,,${S}).$T.o)))

TARGETNAME := $(shell echo ${CORE_SRC} | sed -e 's|[^ ]*/||g' | sed -e 's|\.c||g')
TARGETNAME_MATRIX := $(shell echo ${MATRIX_CORE_SRC} | sed -e 's|[^ ]*/||g' | sed -e 's|\.c||g')
TEST_BIN := $(foreach X,${TARGETNAME},$(foreach Y,$(shell echo ${TESTS} | sed -e 's|[^ ]*\.matrix||g'),${BIN_DIR}/test-$X.$Y))
TEST_BIN_MATRIX := $(foreach C,${TARGETNAME},$(foreach M,${TARGETNAME_MATRIX},$(foreach T,$(filter %.matrix,${TESTS}),${BIN_DIR}/test-$M.$C.$T)))
BENCH_BIN := $(foreach X,${TARGETNAME},$(foreach Y,$(shell echo ${BENCHMARKS} | sed -e 's|[^ ]*\.matrix||g'),${BIN_DIR}/bench-$X.$Y))
BENCH_BIN_MATRIX := $(foreach C,${TARGETNAME},$(foreach M,${TARGETNAME_MATRIX},$(foreach B,$(filter %.matrix,${BENCHMARKS}),${BIN_DIR}/bench-$M.$C.$B)))

.PHONY: verify benchmark clean

${BIN_DIR}/${TARGETNAME}: ${BUILD_DIR}/vect_t-core.o ${UTILS_OBJ} ${BUILD_DIR}/main.o ${UTILS_DIR}/vect_t-utils.h vect_t-core.h | ${BIN_DIR} ${BUILD_DIR}
	gcc ${CFLAGS} -o ${BIN_DIR}/${TARGETNAME} ${BUILD_DIR}/vect_t-core.o ${UTILS_OBJ} ${BUILD_DIR}/main.o vect_t-core.h ${UTILS_DIR}/vect_t-utils.h

${BUILD_DIR}/vect_t-core.o: ${CORE_SRC} vect_t-core.h | ${BUILD_DIR}
	gcc ${CFLAGS} -c -o ${BUILD_DIR}/vect_t-core.o $(firstword ${CORE_SRC})

${BUILD_DIR}/matr_t-%.o: ${MATRIX_UTILS_SRC} ${MATRIX_UTILS_DIR}/matr_t-utils.h | ${BUILD_DIR}
	gcc ${CFLAGS} -c -D VECT_DATATYPE=$(subst .o,,$(lastword $(subst ., ,$(subst .o,,$@)))) -o $@ ${MATRIX_UTILS_DIR}/$(firstword $(subst ., ,$(lastword $(subst /, ,$@)))).c

${BUILD_DIR}/%.o: ${UTILS_SRC} ${UTILS_DIR}/vect_t-utils.h | ${BUILD_DIR}
	gcc ${CFLAGS} -c -D VECT_DATATYPE=$(subst .o,,$(lastword $(subst ., ,$(subst .o,,$@)))) -o $@ ${UTILS_DIR}/$(firstword $(subst ., ,$(lastword $(subst /, ,$@)))).c
	
${BUILD_DIR}/main.o: main.c | ${BUILD_DIR}
	gcc ${CFLAGS} -c -o ${BUILD_DIR}/main.o main.c

${CORE_BUILD_DIR}/%.o: ${CORE_SRC} | ${CORE_BUILD_DIR}
	gcc ${CFLAGS} -c -D VECT_DATATYPE=$(subst .o,,$(lastword $(subst ., ,$(subst .o,,$@)))) -o $@ $(subst ${CORE_BUILD_DIR},${CORE_DIR},$(shell echo $@ | sed -e 's|\..*\.o|\.c|g'))

${MATRIX_CORE_BUILD_DIR}/%.o: ${MATRIX_CORE_SRC} | ${MATRIX_CORE_BUILD_DIR}
	gcc ${CFLAGS} -c -D VECT_DATATYPE=$(subst .o,,$(lastword $(subst ., ,$(subst .o,,$@)))) -o $@ $(subst ${MATRIX_CORE_BUILD_DIR},${MATRIX_CORE_DIR}, $(shell echo $@ | sed -e 's|\..*\.o|\.c|g'))
	
${BUILD_DIR}:
	mkdir -p $@
${BIN_DIR}:
	mkdir -p $@
${TEST_BUILD_DIR}:
	mkdir -p $@
${CORE_BUILD_DIR}:
	mkdir -p $@
${BENCH_BUILD_DIR}:
	mkdir -p $@
${MATRIX_CORE_BUILD_DIR}:
	mkdir -p $@

verify: ${TEST_BIN_MATRIX} ${TEST_BIN}
	@printf "$(shell ./verify.sh '$(patsubst  ,\ ,${TESTS})' $(subst .c,,$(subst ${CORE_DIR}/,,$(firstword ${CORE_SRC}))) $(subst .c,,$(subst ${MATRIX_CORE_DIR}/,,$(firstword ${MATRIX_CORE_SRC}))))"

benchmark: ${BENCH_BIN_MATRIX} ${BENCH_BIN}
	@echo $(shell if [[ `grep avx /proc/cpuinfo | wc -m` -eq 0 ]]; then echo "This machine does not support avx instructions, which is used for reproducible test results. Continue at your own risk. "; fi);\
# 	@printf "./benchmark.sh '$(patsubst  ,\ ,${BENCHMARKS})' '$(subst .c,,$(subst ${CORE_DIR}/,,${CORE_SRC}))' '$(subst .c,,$(subst ${MATRIX_CORE_DIR}/,,${MATRIX_CORE_SRC}))' ${PASSES} '${MATRIX_SIZE}'\n";
	@printf "$(shell ./benchmark.sh '$(patsubst  ,\ ,${BENCHMARKS})' '$(subst .c,,$(subst ${CORE_DIR}/,,${CORE_SRC}))' '$(subst .c,,$(subst ${MATRIX_CORE_DIR}/,,${MATRIX_CORE_SRC}))' ${PASSES} '${MATRIX_SIZE}')"

${BIN_DIR}/test-%.matrix: ${TEST_OBJ} ${MATRIX_CORE_TEST_OBJ} ${MATRIX_UTILS_OBJ} ${CORE_TEST_OBJ} vect_t-core.h ${UTILS_DIR}/vect_t-utils.h ${UTILS_OBJ} | ${BIN_DIR} ${TEST_BUILD_DIR}
	gcc ${CFLAGS} -o $@ ${TEST_BUILD_DIR}/$(shell echo $@ | sed -e 's|[^.]*\.||' | sed -e 's|[^.]*\.||' | sed -e 's|.*/test-||g').o ${CORE_BUILD_DIR}/$(shell echo $@ | sed -e 's|.*/test-||' | sed -e 's|[^.]*\.||' | sed -e 's|\..*||').$(lastword $(subst /, ,$(shell echo $@ | sed -e 's|\.matrix||' | sed -e 's|[^.]*\.||g'))).o ${MATRIX_CORE_BUILD_DIR}/$(shell echo $@ | sed -e 's|${BIN_DIR}/test-||' | sed -e 's|\.matrix||' | sed -e 's|\..*\.|.|g').o $(filter %.$(lastword $(subst ., ,$(subst .matrix,,$@))).o, ${MATRIX_UTILS_OBJ}) $(filter %.$(lastword $(subst ., ,$(subst .matrix,,$@))).o, ${UTILS_OBJ})

${BIN_DIR}/test-%: ${TEST_OBJ} ${CORE_TEST_OBJ} vect_t-core.h ${UTILS_DIR}/vect_t-utils.h ${UTILS_OBJ} | ${BIN_DIR} ${TEST_BUILD_DIR}
	gcc ${CFLAGS} -o $@ ${TEST_BUILD_DIR}/$(shell echo $@ | sed -e 's|[^.]*\.||' | sed -e 's|.*/test-||g').o ${CORE_BUILD_DIR}/$(shell echo $@ | sed -e 's|.*/test-||' | sed -e 's|\..*||').$(lastword $(subst /, ,$(shell echo $@ | sed -e 's|[^.]*\.||g'))).o $(filter %.$(lastword $(subst ., ,$@)).o, ${UTILS_OBJ})
	
${BIN_DIR}/bench-%.matrix: ${BENCH_OBJ} ${MATRIX_CORE_BENCH_OBJ} ${MATRIX_UTILS_OBJ} ${CORE_BENCH_OBJ} vect_t-core.h ${UTILS_DIR}/vect_t-utils.h ${UTILS_OBJ} ${BENCH_DIR}/benchmark-utils.h | ${BIN_DIR} ${BENCH_BUILD_DIR}
	echo "obel";\
	gcc ${CFLAGS} -o $@ ${BENCH_BUILD_DIR}/benchmark-utils.$(shell echo $@ | sed -e 's|\.matrix||' | sed -e 's|.*\.\([^.]*\)|\1|').o ${BENCH_BUILD_DIR}/benchmark-timing.$(shell echo $@ | sed -e 's|\.matrix||' | sed -e 's|.*\.\([^.]*\)|\1|').o ${BENCH_BUILD_DIR}/$(shell echo $@ | sed -e 's|[^.]*\.||' | sed -e 's|[^.]*\.||' | sed -e 's|.*/bench-||g').o ${MATRIX_CORE_BUILD_DIR}/$(shell echo $@ | sed -e 's|.*/bench-||' | sed -e 's|\..*||').$(lastword $(subst /, ,$(shell echo $@ | sed -e 's|\.matrix||' | sed -e 's|[^.]*\.||g'))).o ${CORE_BUILD_DIR}/$(shell echo $@ | sed -e 's|.*/bench-||' | sed -e 's|[^.]*\.||' | sed -e 's|\..*||').$(lastword $(subst /, ,$(shell echo $@ | sed -e 's|\.matrix||' | sed -e 's|[^.]*\.||g'))).o $(filter %.$(lastword $(subst ., ,$(subst .matrix,,$@))).o, ${MATRIX_UTILS_OBJ}) $(filter %.$(lastword $(subst ., ,$(subst .matrix,,$@))).o, ${UTILS_OBJ})

# ${BIN_DIR}/bench-%: ${BENCH_OBJ} ${CORE_BENCH_OBJ} vect_t-core.h ${UTILS_DIR}/vect_t-utils.h ${UTILS_OBJ} ${BENCH_DIR}/benchmark-utils.h | ${BIN_DIR} ${BENCH_BUILD_DIR}
# 	echo "gjhgjgwjgewj";\
# 	gcc ${CFLAGS} -o $@ ${BENCH_BUILD_DIR}/benchmark-utils.$(shell echo $@ | sed -e 's|.*\.\([^.]*\)|\1|').o ${BENCH_BUILD_DIR}/benchmark-timing.$(shell echo $@ | sed -e 's|.*\.\([^.]*\)|\1|').o ${BENCH_BUILD_DIR}/$(shell echo $@ | sed -e 's|[^.]*\.||' | sed -e 's|.*/bench-||g').o ${CORE_BUILD_DIR}/$(shell echo $@ | sed -e 's|.*/bench-||' | sed -e 's|\..*||').$(lastword $(subst /, ,$(shell echo $@ | sed -e 's|[^.]*\.||g'))).o $(filter %.$(lastword $(subst ., ,$@)).o, ${UTILS_OBJ})

${TEST_BUILD_DIR}/%.o: ${TEST_DIR}/%.c | ${TEST_BUILD_DIR}
	gcc ${CFLAGS} -c -D VECT_DATATYPE=$(subst .o,,$(lastword $(subst ., ,$(subst .matrix,,$(subst .o,,$@))))) -o $@ $<

${BENCH_BUILD_DIR}/benchmark-utils.%.o: ${BENCH_DIR}/benchmark-utils.c | ${BENCH_BUILD_DIR}
	gcc ${CFLAGS} -c -D VECT_DATATYPE=$(subst .o,,$(lastword $(subst ., ,$(subst .matrix,,$(subst .o,,$@))))) -o $@ $(subst .int32_t.o,.c,$<)

${BENCH_BUILD_DIR}/benchmark-timing.%.o: ${BENCH_DIR}/benchmark-timing.c | ${BENCH_BUILD_DIR}
	gcc ${CFLAGS} -c -D VECT_DATATYPE=$(subst .o,,$(lastword $(subst ., ,$(subst .matrix,,$(subst .o,,$@))))) -o $@ $(subst .int32_t.o,.c,$<)

${BENCH_BUILD_DIR}/%.o: ${BENCH_DIR}/%.c | ${BENCH_BUILD_DIR}
	gcc ${CFLAGS} -c -D VECT_DATATYPE=$(subst .o,,$(lastword $(subst ., ,$(subst .matrix,,$(subst .o,,$@))))) -o $@ $<
	
# Make this generic
benchmark-reference: ${BENCHMARK_REF_BIN}
	@echo $(shell if [[ `grep avx /proc/cpuinfo | wc -m` -eq 0 ]]; then echo "This machine does not support avx instructions, which is used for reproducible test results. Continue at your own risk. "; fi);\
	printf "$(shell ./benchmark_reference.sh '${BENCHMARK_REF}' ${PASSES} '${MATRIX_SIZE}')"

${BIN_DIR}/reference.%: ${BENCHMARK_REF_SRC} ${BENCH_DIR}/benchmark-timing.c ${REFERENCE_DIR}/reference_helpers.c | ${BIN_DIR}
	gcc ${CFLAGS} -D M_MAX=$(shell echo $@ | sed -e 's|.*\.||' | sed 's|,.*||') -D N_MAX=$(shell echo $@ | sed -e 's|[^,]*,||' | sed -e 's|,[^,]*||') -D P_MAX=$(shell echo $@ | sed -e 's|.*,||') -o $@ $(shell echo $@ | sed -e 's|${BIN_DIR}|${REFERENCE_DIR}|g' | sed -e 's|\.[^.]*$$|\.c|') ${BENCH_DIR}/benchmark-timing.c ${REFERENCE_DIR}/reference_helpers.c

clean:
	rm -r ${BIN_DIR} ${BUILD_DIR} ${TEST_BUILD_DIR} ${CORE_BUILD_DIR} ${MATRIX_CORE_BUILD_DIR} ${BENCH_BUILD_DIR} vgcore.* callgrind.out.* heaptrack.* 2> /dev/null || true

huts:
	echo "${BENCH_OBJ} ${MATRIX_CORE_BENCH_OBJ} ${MATRIX_UTILS_OBJ} ${CORE_BENCH_OBJ} vect_t-core.h ${UTILS_DIR}/vect_t-utils.h ${UTILS_OBJ} ${BENCH_DIR}/benchmark-utils.h | ${BIN_DIR} ${BENCH_BUILD_DIR}"
