#include "../vect_t-core.h"
#include "../vector-utils/vect_t-utils.h"
#include "../matr_t-core.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct MATR_DATA_T {
	uint16_t rc;
	uint64_t *size;
	vect_t *data;
};

typedef struct MATR_DATA_T matr_data_t;

struct MATR_T {
		uint64_t dimensionality;
		uint64_t *size;
		uint64_t *offset;
		matr_data_t *data;
		uint16_t rc;
		VECT_DATATYPE (*get_index_func)(matr_t *, uint64_t[]);
};

VECT_DATATYPE matr_get_vect_index_n(matr_t *m, uint64_t indices[]) {
	uint64_t size_accumulator = 1;
	uint64_t index = 0;
	for (int d = m->dimensionality-1; d >= 0; d--) {
		index += size_accumulator*(indices[d]+m->offset[d]);
		size_accumulator *= m->data->size[d];
	}
	return index;
}

VECT_DATATYPE matr_get_vect_index(matr_t *m, uint64_t indices[]) {
	return indices[0]+m->offset[0]+m->data->size[0]*(indices[1]+m->offset[1]);
}

void matr_inc_rc(matr_t *m, uint16_t amount) {
	inc_rc(m->data->data, amount);
	m->data->rc += amount;
	m->rc += amount;
}

void matr_dec_rc(matr_t *m, uint16_t amount) {
	dec_rc(m->data->data, amount);
	m->data->rc -= amount;
	m->rc -= amount;

	if (m->rc == 0) {
		if (m->data->rc == 0) {
			free(m->data->size);
			free(m->data);
		}
		free(m->size);
		free(m->offset);
		free(m);
	}
}

VECT_DATATYPE matr_get_nr(matr_t *m, uint64_t index[]) {
	uint64_t vect_index = m->get_index_func(m, index);
	return vect_get_nr(m->data->data, vect_index);
}

matr_data_t *matr_alloc_data(uint64_t dimensionality, uint64_t size[]) ;
void matr_set_nr(matr_t *m, uint64_t index[], VECT_DATATYPE elt) {
	if (m->rc != m->data->rc) {
		// copy
		matr_data_t *old_data = m->data;
		old_data->rc -= m->rc;
		dec_rc(old_data->data, 1);

		uint64_t indices[m->dimensionality];
		uint64_t old_offset[m->dimensionality];
		for (uint64_t d=0; d<m->dimensionality; d++) {
			indices[d] = 0;
			old_offset[d] = m->offset[d];
			m->offset[d] = 0;
		}
		m->data = matr_alloc_data(m->dimensionality, m->size);
		while (1) {
			// this can be combine_and_fold
			uint64_t old_vect_index = 0;
			uint64_t old_size_acc = 1;
			uint64_t new_vect_index = 0;
			uint64_t new_size_acc = 1;
			for (int d=0; d<m->dimensionality; d++) {
				old_vect_index += old_size_acc*(indices[d]+old_offset[d]);
				old_size_acc *= old_data->size[d];
				
				new_vect_index += new_size_acc*indices[d];
				new_size_acc *= m->data->size[d];
			}
			vect_set_nr(m->data->data, new_vect_index, vect_get_nr(old_data->data, old_vect_index));
			indices[0]++;
			uint64_t i = 0;
			while (indices[i] >= m->size[i]) {
				if (i >= m->dimensionality-1) goto end;
				indices[i] = 0;
				i++;
				indices[i]++;
			}
		}
		end:; // most normal C syntax
	}
	uint64_t vect_index = m->get_index_func(m, index);
	vect_set_nr(m->data->data, vect_index, elt);
}

uint64_t matr_get_dim_nr(matr_t *m) {
	return m->dimensionality;
}

uint64_t *matr_get_size_nr(matr_t *m) {
	uint64_t *result = malloc(sizeof(uint64_t)*m->dimensionality);
	memcpy(result, m->size, m->dimensionality*sizeof(uint64_t));
	return result;
}

matr_t *matr_alloc_header(uint64_t dimensionality, uint64_t size[]) {
	matr_t *new_matrix = malloc(sizeof(matr_t));
	new_matrix->dimensionality = dimensionality;
	new_matrix->size = malloc(dimensionality*sizeof(uint64_t));
	new_matrix->offset = malloc(dimensionality*sizeof(uint64_t));
	if (dimensionality > 2) {
		new_matrix->get_index_func = matr_get_vect_index_n;
	}
	else {
		new_matrix->get_index_func = matr_get_vect_index;
	}

	for (int d=0; d<dimensionality; d++) {
		new_matrix->size[d] = size[d];
		new_matrix->offset[d] = 0;
	}
	new_matrix->rc = 1;

	return new_matrix;
}

matr_data_t *matr_alloc_data(uint64_t dimensionality, uint64_t size[]) {
	matr_data_t *new_data;
	uint64_t vect_size = 1;

	new_data = malloc(sizeof(matr_data_t));
	new_data->size = malloc(dimensionality*sizeof(uint64_t));

	for (int d=0; d<dimensionality; d++) {
		vect_size *= size[d];
		new_data->size[d] = size[d];
	}
	new_data->rc = 1;

	new_data->data = vect_alloc(vect_size);

	return new_data;
}

matr_t *matr_alloc(uint64_t dimensionality, uint64_t size[]) {
	matr_t *new_matrix = matr_alloc_header(dimensionality, size);
	new_matrix->data = matr_alloc_data(dimensionality, size);
	return new_matrix;
}

matr_t *matr_select_nr(matr_t *source, uint64_t offset[], uint64_t size[]) {
	matr_t *result = matr_alloc_header(source->dimensionality, size);
	if (source->data->rc == 1) {
		// reference
		result->dimensionality = source->dimensionality;
		memcpy(result->size, size, source->dimensionality);
		memcpy(result->offset, offset, source->dimensionality);
		result->data = source->data;
		result->rc = 1;
		result->data->rc+=1;
		for (int i=0; i<source->dimensionality; i++) {
			result->offset[i] = offset[i];
		}
		inc_rc(result->data->data, 1);
	}
	else {
		result->data = matr_alloc_data(source->dimensionality, size);
		// copy
		uint64_t indices[result->dimensionality];
		uint64_t new_indices[result->dimensionality];
		for (int d=0; d<result->dimensionality; d++) {
			indices[d] = offset[d];
			new_indices[d] = 0;
		}
		while (1) {
			matr_set_nr(result, new_indices, matr_get_nr(source, indices));
			int i=0;
			indices[i]++;
			new_indices[i]++;
			while (new_indices[i]>=size[i]) {
				indices[i] = offset[i];
				new_indices[i] = 0;
				i++;
				
				indices[i]++;
				new_indices[i]++;
				
				if (i >= source->dimensionality) goto end;
			}
		}
	}
	end:
	return result;
}

// There is a risk for an overflow in this function, fix this later or build a check
vect_t *matr_get_vect(matr_t *m, uint64_t offset[], uint64_t size) {
	uint64_t vect_offset = 0;
	uint64_t size_acc = 1;
	for (int d=0; d<m->dimensionality; d++) {
		vect_offset += size_acc*offset[d];
		size_acc *= m->size[d];
	}
	inc_rc(m->data->data, 1);
	vect_t *result = vect_select(m->data->data, vect_offset, size);
	matr_dec_rc(m, 1);
	return result;
}
