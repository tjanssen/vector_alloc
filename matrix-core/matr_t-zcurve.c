#include "../vect_t-core.h"
#include "../vector-utils/vect_t-utils.h"
#include "../matr_t-core.h"
#include <stdlib.h>
#include <string.h>

struct MATR_T {
		uint64_t dimensionality;
		uint64_t *size;
		uint64_t row_alloc_size;
		vect_t *data;
		uint16_t rc;
};

VECT_DATATYPE matr_get_vect_index(matr_t *m, uint64_t indices[]) {
	uint64_t z = 0;
	for (int b=0; b < m->row_alloc_size * 8; b++) {
		for (int i=0; i < m->dimensionality; i++) {
			z |= (indices[i] & (1U << b)) << (i+b*(m->dimensionality-1));
		}
	}
	return z;
}

void matr_inc_rc(matr_t *m, uint16_t amount) {
	inc_rc(m->data, amount);
	m->rc += amount;
}

void matr_dec_rc(matr_t *m, uint16_t amount) {
	dec_rc(m->data, amount);
	m->rc -= amount;

	if (m->rc == 0) {
		free(m->size);
		free(m);
	}
}

VECT_DATATYPE matr_get_nr(matr_t *m, uint64_t index[]) {
	uint64_t vect_index = matr_get_vect_index(m, index);
	return vect_get_nr(m->data, vect_index);
}

void matr_set_nr(matr_t *m, uint64_t index[], VECT_DATATYPE elt) {
	uint64_t vect_index = matr_get_vect_index(m, index);
	vect_set_nr(m->data, vect_index, elt);
}

uint64_t matr_get_dim_nr(matr_t *m) {
	return m->dimensionality;
}

uint64_t *matr_get_size_nr(matr_t *m) {
	uint64_t *result = malloc(sizeof(uint64_t)*m->dimensionality);
	memcpy(result, m->size, m->dimensionality*sizeof(uint64_t));
	return result;
}

matr_t *matr_alloc(uint64_t dimensionality, uint64_t size[]) {
	uint64_t vect_size = 1;

	matr_t *new_matrix = malloc(sizeof(matr_t));
	new_matrix->dimensionality = dimensionality;
	new_matrix->size = malloc(dimensionality*sizeof(uint64_t));
	new_matrix->row_alloc_size = 0;

	for (int d=0; d<dimensionality; d++) {
		uint64_t v = size[d];
		v--;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v++;
		if (v >= new_matrix->row_alloc_size) {
			new_matrix->row_alloc_size = v;
		}
		new_matrix->size[d] = size[d];
	}
	new_matrix->data = vect_alloc(new_matrix->row_alloc_size * new_matrix->row_alloc_size);
	new_matrix->rc = 1;

	return new_matrix;
}

matr_t *matr_select_nr(matr_t *source, uint64_t offset[], uint64_t size[]) {
	matr_t *new_matrix = matr_alloc(source->dimensionality, size);
	uint64_t indices[new_matrix->dimensionality];
	uint64_t new_indices[new_matrix->dimensionality];
	for (int d=0; d<new_matrix->dimensionality; d++) {
		indices[d] = offset[d];
		new_indices[d] = 0;
	}
	while (1) {
		matr_set_nr(new_matrix, new_indices, matr_get_nr(source, indices));
		int i=0;
		indices[i]++;
		new_indices[i]++;
		while (new_indices[i]>=size[i]) {
			indices[i] = offset[i];
			new_indices[i] = 0;
			i++;

			indices[i]++;
			new_indices[i]++;

			if (i >= source->dimensionality) goto end;
		}
	}
	end:
	return new_matrix;
}
