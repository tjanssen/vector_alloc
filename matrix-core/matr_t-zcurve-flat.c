#include "../matr_t-core.h"
#include <stdlib.h>
#include <string.h>

struct MATR_DATA_T {
	uint16_t rc;
	// uint64_t *size;
	VECT_DATATYPE *data;
	uint64_t *lookup_table_x;
	uint64_t *lookup_table_y;
};
typedef struct MATR_DATA_T matr_data_t;

struct MATR_T {
		uint64_t dimensionality;
		uint64_t *size;
		uint64_t row_alloc_size;
		uint64_t *offset;
		matr_data_t *data;
		uint16_t rc;
};

uint64_t matr_get_vect_index(matr_t *m, uint64_t indices[]) {
	uint64_t result;
	result = m->data->lookup_table_x[indices[0]+m->offset[0]] | m->data->lookup_table_y[indices[1]+m->offset[1]];
	return result;
}

void matr_inc_rc(matr_t *m, uint16_t amount) {
	m->data->rc += amount;
	m->rc += amount;
}

void matr_dec_rc(matr_t *m, uint16_t amount) {
	m->data->rc -= amount;
	m->rc -= amount;

	if (m->rc == 0) {
		if (m->data->rc == 0) {
			// free(m->data->size);
			free(m->data->lookup_table_x);
			free(m->data->lookup_table_y);
			free(m->data->data);
			free(m->data);
		}
		free(m->offset);
		free(m->size);
		free(m);
	}
}

matr_t *matr_alloc_header(uint64_t dimensionality, uint64_t *size) {
	uint64_t vect_size = 1;

	matr_t *new_matrix = malloc(sizeof(matr_t));
	new_matrix->dimensionality = dimensionality;
	new_matrix->size = malloc(dimensionality*sizeof(uint64_t));
	new_matrix->row_alloc_size = 0;
	new_matrix->offset = malloc(dimensionality*sizeof(uint64_t));

	for (int d=0; d<dimensionality; d++) {
		uint64_t v = size[d];
		v--;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v++;
		if (v >= new_matrix->row_alloc_size) {
			new_matrix->row_alloc_size = v;
		}
		new_matrix->size[d] = size[d];
		new_matrix->offset[d] = 0;
	}
	new_matrix->rc = 1;
	new_matrix->data = NULL;
	
	return new_matrix;
}

// this needs to be 64 bits since some shifting will happen
// assume 2d for now
void fill_lookup(matr_t *m) {
	// change the allocation sizes from m->row_alloc_size to m->size[n] if normal
	// size matrices are used. 
	m->data->lookup_table_x = malloc(m->row_alloc_size*sizeof(uint64_t));
	m->data->lookup_table_y = malloc(m->row_alloc_size*sizeof(uint64_t));
	
	for (uint64_t i = 0; i < m->row_alloc_size; i++) {
		uint64_t position = 0;
		uint64_t result = 0;
		uint64_t inp = i;
		while (inp > 0) {
			result |= (inp & 1) << position;
			position += 2;
			inp >>= 1;
		}
		m->data->lookup_table_x[i] = result;
	}

	for (uint64_t i = 0; i < m->row_alloc_size; i++) {
		uint64_t position = 1;
		uint64_t result = 0;
		uint64_t inp = i;
		while (inp > 0) {
			result |= (inp & 1) << position;
			position += 2;
			inp >>= 1;
		}
		m->data->lookup_table_y[i] = result;
	}
}

// currently only square matrices supported
matr_data_t *matr_alloc_data(uint64_t dimensionality, uint64_t alloc_size) {
	matr_data_t *result = malloc(sizeof(matr_data_t));
	result->data = NULL;
	// result->size = malloc(dimensionality*sizeof(uint64_t)); // currently this isn't used for anything
	result->rc = 1;
	return result;
}

VECT_DATATYPE matr_get_nr(matr_t *m, uint64_t index[]) {
	uint64_t vect_index = matr_get_vect_index(m, index);
	return m->data->data[vect_index];
}

void matr_set_nr(matr_t *m, uint64_t index[], VECT_DATATYPE elt) {
	if (m->rc == m->data->rc) {
		uint64_t vect_index = matr_get_vect_index(m, index);
		m->data->data[vect_index] =  elt;
	}
	else {
		uint64_t vect_size = 1;
		for (int d=0; d<m->dimensionality; d++) {
			vect_size *= m->row_alloc_size; // change this to per row in case this is supported
		}
		m->data->rc -= m->rc;
		VECT_DATATYPE *old_vect = m->data->data;
		m->data = matr_alloc_data(m->dimensionality, m->row_alloc_size);
		m->data->data = malloc(vect_size*sizeof(VECT_DATATYPE));
		memcpy(m->data->data, old_vect, vect_size*sizeof(VECT_DATATYPE));
		m->data->rc = m->rc;
		fill_lookup(m);
		uint64_t vect_index = matr_get_vect_index(m, index);
		m->data->data[vect_index] = elt;
	}
}

uint64_t matr_get_dim_nr(matr_t *m) {
	return m->dimensionality;
}

uint64_t *matr_get_size_nr(matr_t *m) {
	uint64_t *result = malloc(sizeof(uint64_t)*m->dimensionality);
	memcpy(result, m->size, m->dimensionality*sizeof(uint64_t));
	return result;
}

matr_t *matr_alloc(uint64_t dimensionality, uint64_t size[]) {
	matr_t *new_matrix = matr_alloc_header(dimensionality, size);
	new_matrix->data = matr_alloc_data(dimensionality, new_matrix->row_alloc_size);
	uint64_t vect_size = 1;
	
	// TODO: Make allocations respect size in each dimensionality instead of just taking the largest one
	for (int d=0; d<dimensionality; d++) {
		vect_size *= new_matrix->row_alloc_size;
	}
	new_matrix->data->data = malloc(vect_size*sizeof(VECT_DATATYPE));
	fill_lookup(new_matrix);

	return new_matrix;
}

matr_t *matr_select_nr(matr_t *source, uint64_t offset[], uint64_t size[]) {
	matr_t *new_matrix = matr_alloc_header(source->dimensionality, size);
	for (int d=0; d<new_matrix->dimensionality; d++) {
		new_matrix->offset[d] = offset[d]+source->offset[d];
	}
	new_matrix->data = source->data;
	new_matrix->data->rc++;
	
	return new_matrix;
}
