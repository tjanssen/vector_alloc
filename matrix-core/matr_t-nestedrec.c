#include "../vect_t-core.h"
#include "../vector-utils/vect_t-utils.h"
#include "../matr_t-core.h"
#include <stdlib.h>
#include <string.h>

struct DATA_T {
	uint64_t refs_size; // This functions as a safeguard mostly
	struct DATA_T **refs;
	vect_t *payload;
	uint16_t rc;
};

typedef struct DATA_T matr_data_t;

struct MATR_T {
	matr_data_t *top_ref;
	uint64_t *refs_offset_start;
	uint64_t *refs_offset_end;
	uint64_t dimensionality;
	uint16_t rc;
};

void matr_inc_rc_data(matr_data_t *d, uint16_t amount, uint64_t *offset_start, uint64_t *offset_end, uint64_t depth) {
	d->rc += amount;
	if (depth > 1) {
		for (int i=offset_start[depth-1]; i<offset_end[depth-1]; i++) {
			matr_inc_rc_data(d->refs[i], amount, offset_start, offset_end, depth-1);
		}
	}
	else {
		inc_rc(d->payload, amount);
	}
}

void matr_inc_rc(matr_t *m, uint16_t amount) {
	m->rc += amount;
	matr_inc_rc_data(m->top_ref, amount, m->refs_offset_start, m->refs_offset_end, m->dimensionality);
}

void matr_dec_rc_data(matr_data_t *d, uint16_t amount, uint64_t *offset_start, uint64_t *offset_end, uint64_t depth) {
	d->rc -= amount;
	if (depth > 1) {
		for (int i=offset_start[depth-1]; i<offset_end[depth-1]; i++) {
			matr_dec_rc_data(d->refs[i], amount, offset_start, offset_end, depth-1);
		}
	}
	else {
		dec_rc(d->payload, amount);
	}
	if (d->rc == 0) {
		if (depth > 0) free(d->refs);
		free(d);
	}
}

void matr_dec_rc(matr_t *m, uint16_t amount) {
	m->rc -= amount;
	matr_dec_rc_data(m->top_ref, amount, m->refs_offset_start, m->refs_offset_end, m->dimensionality);
	if (m->rc == 0) {
		free(m->refs_offset_start);
		free(m->refs_offset_end);
		free(m);
	}
}

VECT_DATATYPE matr_get_nr_rec(matr_data_t *d, uint64_t index[], uint64_t *offset_start, uint64_t depth) {
	if (depth == 1) return vect_get_nr(d->payload, index[0]+offset_start[0]);
	else return matr_get_nr_rec(d->refs[index[depth-1]+offset_start[depth-1]], index, offset_start, depth-1);
}

// Maybe zip index and offset_start in this function so they don't need separate args. 
VECT_DATATYPE matr_get_nr(matr_t *m, uint64_t index[]) {
	return matr_get_nr_rec(m->top_ref, index, m->refs_offset_start, m->dimensionality);
}

void matr_set_nr_rec(matr_data_t *d, uint64_t index[], VECT_DATATYPE elt, uint64_t *offset_start, uint64_t depth) {
	if (depth == 1) vect_set_nr(d->payload, index[0]+offset_start[0], elt);
	else matr_set_nr_rec(d->refs[index[depth-1]+offset_start[depth-1]], index, elt, offset_start, depth-1);
}

void matr_set_nr(matr_t *m, uint64_t index[], VECT_DATATYPE elt) {
	matr_set_nr_rec(m->top_ref, index, elt, m->refs_offset_start, m->dimensionality);
}

uint64_t matr_get_dim_nr(matr_t *m) {
	return m->dimensionality;
}

void matr_get_size_nr_rec(matr_data_t *d, uint64_t *offset_start, uint64_t *offset_end, uint64_t depth, uint64_t *buf) {
	if (depth > 1) {
		// TODO: this doesn't need a separate allocation for each recursion
		matr_get_size_nr_rec(d->refs[offset_start[depth-1]], offset_start, offset_end, depth-1, buf);
	}
	buf[depth-1] = (offset_end[depth-1])-(offset_start[depth-1]);
}

uint64_t *matr_get_size_nr(matr_t *m) {
	uint64_t *buf = malloc(4*m->dimensionality);
	matr_get_size_nr_rec(m->top_ref, m->refs_offset_start, m->refs_offset_end, m->dimensionality, buf);
	return buf;
}

matr_data_t *matr_alloc_rec(uint64_t dimensionality, uint64_t size[]) {
	matr_data_t *result = malloc(sizeof(matr_data_t));
	result->rc=1;
	if (dimensionality > 1) {
		result->refs = malloc(size[dimensionality-1]*sizeof(matr_data_t));
		for (int i=0; i<size[dimensionality-1]; i++) {
			result->refs[i] = matr_alloc_rec(dimensionality-1, size);
		}
		result->refs_size = size[dimensionality-1];
	}
	else {
		result->refs = NULL;
		result->refs_size = 0;
		result->payload = vect_alloc(size[0]);
	}
	return result;
}

matr_t *matr_alloc(uint64_t dimensionality, uint64_t size[]) {
	matr_t *result = malloc(sizeof(matr_t));
	result->dimensionality = dimensionality;
	if (dimensionality > 0) {
		result->top_ref = matr_alloc_rec(dimensionality, size);
		result->refs_offset_start = calloc(dimensionality, sizeof(uint64_t));
		result->refs_offset_end = calloc(dimensionality, sizeof(uint64_t));
		result->dimensionality = dimensionality;
		for (int d=0; d<dimensionality; d++) {
			result->refs_offset_end[d] = size[d];
		}
	}
	result->rc = 1;
	return result;
}

matr_t *matr_select_nr(matr_t *source, uint64_t offset[], uint64_t size[]) {
	matr_t *result = malloc(sizeof(matr_t));

	result->dimensionality = source->dimensionality;
	result->top_ref = source->top_ref;
	result->refs_offset_start = malloc(sizeof(uint64_t)*source->dimensionality);
	result->refs_offset_end = malloc(sizeof(uint64_t)*source->dimensionality);
	for (int i=0; i<source->dimensionality; i++) {
		result->refs_offset_start[i] = source->refs_offset_start[i] + offset[i];
		result->refs_offset_end[i] = source->refs_offset_start[i] + offset[i] + size[i];
	}
	// TODO: maybe do this more elegantly
	matr_inc_rc(result, 1);
	result->rc = 1;
	return result;
}
