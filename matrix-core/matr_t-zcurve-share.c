#include "../vect_t-core.h"
#include "../vector-utils/vect_t-utils.h"
#include "../matr_t-core.h"
#include <stdlib.h>
#include <string.h>

struct MATR_DATA_T {
	uint16_t rc;
	// uint64_t *size;
	vect_t *data;
	uint64_t *lookup_table_x;
	uint64_t *lookup_table_y;
};
typedef struct MATR_DATA_T matr_data_t;

struct MATR_T {
		uint64_t dimensionality;
		uint64_t *size;
		uint64_t row_alloc_size;
		uint64_t *offset;
		matr_data_t *data;
		uint16_t rc;
};

// VECT_DATATYPE matr_get_vect_index(matr_t *m, uint64_t indices[]) {
// 	uint64_t z = 0;
// 	for (int b=0; b < m->row_alloc_size * 8; b++) {
// 		for (int i=0; i < m->dimensionality; i++) {
// 			z |= (indices[i]+m->offset[i] & (1U << b)) << (i+b*(m->dimensionality-1));
// 		}
// 	}
// 	return z;
// }

uint64_t matr_get_vect_index(matr_t *m, uint64_t indices[]) {
	uint64_t result;
	result = m->data->lookup_table_x[indices[0]+m->offset[0]] | m->data->lookup_table_y[indices[1]+m->offset[1]];
	return result;
}

void matr_inc_rc(matr_t *m, uint16_t amount) {
	inc_rc(m->data->data, amount);
	m->data->rc += amount;
	m->rc += amount;
}

void matr_dec_rc(matr_t *m, uint16_t amount) {
	dec_rc(m->data->data, amount);
	m->data->rc -= amount;
	m->rc -= amount;

	if (m->rc == 0) {
		if (m->data->rc == 0) {
			// free(m->data->size);
			free(m->data->lookup_table_x);
			free(m->data->lookup_table_y);
			free(m->data);
		}
		free(m->offset);
		free(m->size);
		free(m);
	}
}

matr_t *matr_alloc_header(uint64_t dimensionality, uint64_t *size) {
	uint64_t vect_size = 1;

	matr_t *new_matrix = malloc(sizeof(matr_t));
	new_matrix->dimensionality = dimensionality;
	new_matrix->size = malloc(dimensionality*sizeof(uint64_t));
	new_matrix->row_alloc_size = 0;
	new_matrix->offset = malloc(dimensionality*sizeof(uint64_t));

	for (int d=0; d<dimensionality; d++) {
		uint64_t v = size[d];
		v--;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v++;
		if (v >= new_matrix->row_alloc_size) {
			new_matrix->row_alloc_size = v;
		}
		new_matrix->size[d] = size[d];
		new_matrix->offset[d] = 0;
	}
	new_matrix->rc = 1;
	new_matrix->data = NULL;
	
	return new_matrix;
}

// this needs to be 64 bits since some shifting will happen
// assume 2d for now
void fill_lookup(matr_t *m) {
	// change the allocation sizes from m->row_alloc_size to m->size[n] if normal
	// size matrices are used. 
	m->data->lookup_table_x = malloc(m->row_alloc_size*sizeof(uint64_t));
	m->data->lookup_table_y = malloc(m->row_alloc_size*sizeof(uint64_t));
	
	for (uint64_t i = 0; i < m->row_alloc_size; i++) {
		uint64_t position = 0;
		uint64_t result = 0;
		uint64_t inp = i;
		while (inp > 0) {
			result |= (inp & 1) << position;
			position += 2;
			inp >>= 1;
		}
		m->data->lookup_table_x[i] = result;
	}

	for (uint64_t i = 0; i < m->row_alloc_size; i++) {
		uint64_t position = 1;
		uint64_t result = 0;
		uint64_t inp = i;
		while (inp > 0) {
			result |= (inp & 1) << position;
			position += 2;
			inp >>= 1;
		}
		m->data->lookup_table_y[i] = result;
	}
}

// currently only square matrices supported
matr_data_t *matr_alloc_data(uint64_t dimensionality, uint64_t alloc_size) {
	matr_data_t *result = malloc(sizeof(matr_data_t));
	result->data = NULL;
	// result->size = malloc(dimensionality*sizeof(uint64_t)); // currently this isn't used for anything
	result->rc = 1;
	return result;
}

VECT_DATATYPE matr_get_nr(matr_t *m, uint64_t index[]) {
	uint64_t vect_index = matr_get_vect_index(m, index);
	return vect_get_nr(m->data->data, vect_index);
}

void matr_set_nr(matr_t *m, uint64_t index[], VECT_DATATYPE elt) {
	if (m->rc == m->data->rc) {
		uint64_t vect_index = matr_get_vect_index(m, index);
		vect_set_nr(m->data->data, vect_index, elt);
	}
	else {
		uint64_t vect_size = 1;
		for (int d=0; d<m->dimensionality; d++) {
			vect_size *= m->row_alloc_size; // change this to per row in case this is supported
		}
		m->data->rc -= m->rc;
		vect_t *old_vect = m->data->data;
		m->data = matr_alloc_data(m->dimensionality, m->row_alloc_size);
		m->data->data = vect_select_nr(old_vect, 0, vect_size);
		dec_rc(old_vect, m->rc);
		m->data->rc = m->rc;
		inc_rc(m->data->data, m->rc-1);
		fill_lookup(m);
		uint64_t vect_index = matr_get_vect_index(m, index);
		vect_set_nr(m->data->data, vect_index, elt);
		// m->data->data = vect_select(old_vect, matr_get_vect_index(m, index), vect_size);
		// uint64_t indices[new_matrix->dimensionality];
		// uint64_t new_indices[new_matrix->dimensionality];
		// for (int d=0; d<new_matrix->dimensionality; d++) {
		// 	indices[d] = offset[d];
		// 	new_indices[d] = 0;
		// }
		// while (1) {
		// 	matr_set_nr(new_matrix, new_indices, matr_get_nr(source, indices));
		// 	int i=0;
		// 	indices[i]++;
		// 	new_indices[i]++;
		// 	while (new_indices[i]>=size[i]) {
		// 		indices[i] = offset[i];
		// 		new_indices[i] = 0;
		// 		i++;
  // 
		// 		indices[i]++;
		// 		new_indices[i]++;
  // 
		// 		if (i >= source->dimensionality) goto end;
		// 	}
		// }
		// end:
	}
}

uint64_t matr_get_dim_nr(matr_t *m) {
	return m->dimensionality;
}

uint64_t *matr_get_size_nr(matr_t *m) {
	uint64_t *result = malloc(sizeof(uint64_t)*m->dimensionality);
	memcpy(result, m->size, m->dimensionality*sizeof(uint64_t));
	return result;
}

matr_t *matr_alloc(uint64_t dimensionality, uint64_t size[]) {
	matr_t *new_matrix = matr_alloc_header(dimensionality, size);
	new_matrix->data = matr_alloc_data(dimensionality, new_matrix->row_alloc_size);
	new_matrix->data->data = vect_alloc(new_matrix->row_alloc_size * new_matrix->row_alloc_size);
	fill_lookup(new_matrix);

	return new_matrix;
}

matr_t *matr_select_nr(matr_t *source, uint64_t offset[], uint64_t size[]) {
	matr_t *new_matrix = matr_alloc_header(source->dimensionality, size);
	for (int d=0; d<new_matrix->dimensionality; d++) {
		new_matrix->offset[d] = offset[d]+source->offset[d];
	}
	new_matrix->data = source->data;
	new_matrix->data->rc++;
	inc_rc(new_matrix->data->data, 1);
	
	return new_matrix;
}
