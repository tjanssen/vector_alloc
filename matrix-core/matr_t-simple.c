#include "../vect_t-core.h"
#include "../vector-utils/vect_t-utils.h"
#include "../matr_t-core.h"
#include <stdlib.h>
#include <string.h>

struct MATR_T {
		uint64_t size_x;
		uint64_t size_y;
		vect_t *data;
		uint16_t rc;
};

void matr_inc_rc(matr_t *m, uint16_t amount) {
	inc_rc(m->data, amount);
	m->rc += amount;
}

void matr_dec_rc(matr_t *m, uint16_t amount) {
	dec_rc(m->data, amount);
	m->rc -= amount;

	if (m->rc == 0) {
		free(m);
	}
}

uint64_t matr_get_vect_index(matr_t *m, uint64_t indices[]) {
	uint64_t size_accumulator = 1;
	uint64_t index = 0;
	index = indices[0]+m->size_x*indices[1];
	return index;
}

VECT_DATATYPE matr_get_nr(matr_t *m, uint64_t index[]) {
	uint64_t vect_index = matr_get_vect_index(m, index);
	return vect_get_nr(m->data, vect_index);
}

void matr_set_nr(matr_t *m, uint64_t index[], VECT_DATATYPE elt) {
	uint64_t vect_index = matr_get_vect_index(m, index);
	vect_set_nr(m->data, vect_index, elt);
}

uint64_t matr_get_dim_nr(matr_t *m) {
	return 2;
}

uint64_t *matr_get_size_nr(matr_t *m) {
	uint64_t *result = malloc(sizeof(uint64_t)*2);
	result[0] = m->size_x;
	result[1] = m->size_y;
	return result;
}

matr_t *matr_alloc(uint64_t dimensionality, uint64_t size[]) {
	uint64_t vect_size = 1;

	matr_t *new_matrix = malloc(sizeof(matr_t));
	new_matrix->size_x = size[0];
	new_matrix->size_y = size[1];

	vect_size = size[0]*size[1];
	new_matrix->data = vect_alloc(vect_size);
	new_matrix->rc = 1;

	return new_matrix;
}

matr_t *matr_select_nr(matr_t *source, uint64_t offset[], uint64_t size[]) {
	matr_t *new_matrix = matr_alloc(2, size);
	uint64_t indices[2];
	uint64_t new_indices[2];
	for (int d=0; d<2; d++) {
		indices[d] = offset[d];
		new_indices[d] = 0;
	}
	while (1) {
		matr_set_nr(new_matrix, new_indices, matr_get_nr(source, indices));
		int i=0;
		indices[i]++;
		new_indices[i]++;
		while (new_indices[i]>=size[i]) {
			indices[i] = offset[i];
			new_indices[i] = 0;
			i++;
			
			if (i >= 2) goto end;
			indices[i]++;
			new_indices[i]++;
			
		}
	}
	end:
	return new_matrix;
}
