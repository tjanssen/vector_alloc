#include "../vect_t-core.h"
#include "../vector-utils/vect_t-utils.h"
#include "../matr_t-core.h"
#include <stdlib.h>
#include <string.h>

struct MATR_T {
	VECT_DATATYPE **data;
	uint64_t size_x;
	uint64_t size_y;
	uint16_t rc;
};

void matr_inc_rc(matr_t *m, uint16_t amount) {
	m->rc += amount;
}

void matr_dec_rc(matr_t *m, uint16_t amount) {
	m->rc -= amount;

	if (m->rc == 0) {
		for (uint64_t y=0; y<m->size_y; y++) {
			free(m->data[y]);
		}
		free(m->data);
		free(m);
	}
}

// Maybe zip index and offset_start in this function so they don't need separate args. 
VECT_DATATYPE matr_get_nr(matr_t *m, uint64_t index[]) {
	return m->data[index[1]][index[0]];
}

void matr_set_nr(matr_t *m, uint64_t index[], VECT_DATATYPE elt) {
	m->data[index[1]][index[0]] = elt;
}

uint64_t matr_get_dim_nr(matr_t *m) {
	return 2;
}

uint64_t *matr_get_size_nr(matr_t *m) {
	uint64_t *buf = malloc(sizeof(uint64_t)*2);
	buf[0] = m->size_x;
	buf[1] = m->size_y;
	return buf;
}

matr_t *matr_alloc(uint64_t dimensionality, uint64_t size[]) {
	matr_t *result = malloc(sizeof(matr_t));
	result->size_x = size[0];
	result->size_y = size[1];
	result->data = malloc(sizeof(uintptr_t)*size[1]);
	for (uint64_t y=0; y<size[1]; y++) {
		result->data[y] = malloc(sizeof(VECT_DATATYPE)*size[0]);
	}
	result->rc=1;
	return result;
}

matr_t *matr_select_nr(matr_t *source, uint64_t offset[], uint64_t size[]) {
	matr_t *result = matr_alloc(2, size);
	for (uint64_t y=0; y<size[1]; y++) {
		for (uint64_t x=0; x<size[0]; x++) {
			result->data[y][x] = source->data[y+offset[1]][x+offset[0]];
		}
	}
	return result;
}
