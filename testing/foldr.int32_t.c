#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <stdio.h>

#define VECT_SIZE 10

VECT_DATATYPE __product__(VECT_DATATYPE n1, VECT_DATATYPE n2) {
	return n1*n2;
}

int main() {
	vect_t *v = vect_alloc(VECT_SIZE);
	for (int i=0; i<VECT_SIZE; i++) {
		inc_rc(v, 1);
		vect_set(v, i, i+1);
	}
	VECT_DATATYPE result = vect_foldr(__product__, 1, v);
	printf("%d\n", result);
}
