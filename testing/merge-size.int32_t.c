#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <stddef.h>
#include <stdio.h>

int main() {
	vect_t *v1 = vect_alloc(1);
	inc_rc(v1, 1);
	vect_set(v1, 0, 1);
	
  vect_t *v2 = vect_alloc(1);
	inc_rc(v2, 1);
	vect_set(v2, 0, 2);
	
  vect_t *v_combined = vect_insert(v1, v2, 0);
	uint32_t v_size = vect_get_size(v_combined);
  printf("%d\n", v_size);
	return 0;
}
