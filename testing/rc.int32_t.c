#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <stddef.h>

int main() {
	vect_t *v = vect_alloc(1);
	dec_rc(v, 1);
	return 0;
}
