#include "../matrix-utils/matr_t-utils.h"
#include "../matr_t-core.h"
#include <stddef.h>
#include <stdio.h>

#define SIZE_X 2
#define SIZE_Y 3

int main() {
	uint64_t size[] = {SIZE_X,SIZE_Y};
	matr_t *m1 = matr_alloc(2, size);
	int value = 0;
	for (int y=0; y<SIZE_Y; y++) {
		for (int x=0; x<SIZE_X; x++) {
			matr_inc_rc(m1, 1);
			uint64_t index[] = {x, y};
			matr_set(m1, index, value);
			value++;
		}
	}
	matr_basic_print(matr_transpose(m1));
	return 0;
}
