#include <stdio.h>
#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"

// this may overflow but that doesn't matter since this is a test anyway
VECT_DATATYPE counter_lmao = -1;

VECT_DATATYPE epic_setter(VECT_DATATYPE input[], uint32_t argc) {
	counter_lmao++;
	return counter_lmao;
}

int main() {
	uint64_t source_matrix_size[] = {5,5};
	matr_t *source_matrix = matr_alloc(2, source_matrix_size);
	source_matrix = matr_map(epic_setter, &source_matrix, 1);
	
	uint64_t copy_matrix_size[] = {3,3};
	matr_t *copy_matrix = matr_alloc(2, copy_matrix_size);
	counter_lmao = 99;
	copy_matrix = matr_map(epic_setter, &copy_matrix, 1);
	
	uint64_t offset[] = {2,1};
	matr_t *result = matr_copy(source_matrix, copy_matrix, offset);
	
	matr_basic_print(result);
	
	return 0;
}
