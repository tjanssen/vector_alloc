#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <stddef.h>
#include <stdio.h>

int main() {
	vect_t *v1 = vect_insert(vect_alloc(2), vect_alloc(2), 0);
  inc_rc(v1, 6);
  vect_set(v1, 0, 1);
  vect_set(v1, 1, 2);
  vect_set(v1, 2, 3);
  vect_set(v1, 3, 4);
  
	vect_t *v2 = vect_insert(vect_alloc(2), vect_alloc(2), 0);
  inc_rc(v2, 6);
  vect_set(v2, 0, 5);
  vect_set(v2, 1, 6);
  vect_set(v2, 2, 7);
  vect_set(v2, 3, 8);
	
  printf("%d\n", vect_get(v1, 0));
	printf("%d\n", vect_get(v2, 1));
  printf("%d\n", vect_get(v1, 2));
	printf("%d\n", vect_get(v2, 3));
	dec_rc(v1, 1);
	dec_rc(v2, 1);
	return 0;
}
