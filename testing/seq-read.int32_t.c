#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <stddef.h>
#include <stdio.h>

int main() {
	vect_t *v1 = vect_alloc(2);
  inc_rc(v1, 2);
  vect_set(v1, 0, 1);
  vect_set(v1, 1, 2);
  
  vect_t *v2 = vect_alloc(2);
  inc_rc(v2, 2);
  vect_set(v2, 0, 3);
  vect_set(v2, 1, 4);
	
  vect_t *v_combined = vect_insert(v1, v2, 0);
	inc_rc(v_combined, 2);
	VECT_DATATYPE result0 = vect_get(v_combined, 1);
	VECT_DATATYPE result1 = vect_get(v_combined, 2);
  printf("%d\n", result0);
	printf("%d\n", result1);
  dec_rc(v_combined, 1);
	return 0;
}
