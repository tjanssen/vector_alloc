#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <stddef.h>
#include <stdio.h>

#define VECT_DATATYPE double

int main() {
	vect_t *v1 = vect_alloc(1);
	inc_rc(v1, 1);
	vect_set(v1, 0, 3.14);
	printf("%.2f\n", vect_get(v1, 0));
	return 0;
}
