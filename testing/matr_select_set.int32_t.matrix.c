#include <stdio.h>
#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"

// this may overflow but that doesn't matter since this is a test anyway
VECT_DATATYPE counter_lmao = -1;

VECT_DATATYPE epic_setter(VECT_DATATYPE input[], uint32_t argc) {
	counter_lmao++;
	return counter_lmao;
}

int main() {
	uint64_t source_matrix_size[] = {5,5};
	matr_t *source_matrix = matr_alloc(2, source_matrix_size);
	source_matrix = matr_map(epic_setter, &source_matrix, 1);
	
	uint64_t selection_offset[] = {1,1};
	uint64_t selection_size[] = {3,3};
	matr_t *submatrix = matr_select_nr(source_matrix, selection_offset, selection_size);

	matr_set_nr(submatrix, selection_offset, 100);
	
	matr_basic_print(submatrix);

	matr_dec_rc(source_matrix, 1);
	
	return 0;
}
