// Check the file ../masked_image.pgm whether it has a convincing box blur.

#include "../matrix-utils/matr_t-utils.h"
#include "../matr_t-core.h"

VECT_DATATYPE return_zero(VECT_DATATYPE *not_needed, uint32_t argc) {
	return 0;
}

int main() {
	matr_t *image = image_read("testing/extras/hello.pgm");
	uint64_t mask_size[] = {3,3};
	matr_t *mask = matr_alloc(2, mask_size);
	uint64_t index[2];
	for (index[0] = 0; index[0] < 3; index[0]++) {
		for (index[1] = 0; index[1] < 3; index[1]++) {
			matr_set_nr(mask, index, 1);
		}
	}
	matr_t *masked_image = image_mask(image, mask, 9);
	image_write(masked_image, "../masked_image.pgm");
	return 11;
}
