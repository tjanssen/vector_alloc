#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <stddef.h>
#include <stdio.h>

int main() {
	vect_t *v = vect_alloc(1);
	inc_rc(v, 1);
	vect_set(v, 0, 1);
	
	vect_t *w = vect_select(v, 0, 1);
	vect_basic_print(w);
	
	return 0;
}
