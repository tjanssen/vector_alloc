#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <stddef.h>
#include <stdio.h>

char is_even(VECT_DATATYPE a) {
	return a%2-1;
}

int main() {
	vect_t *v = vect_alloc(10);
	inc_rc(v, 10);
	for (int i=0; i<10; i++) {
		vect_set(v, i, i);
	}
	vect_t *w = vect_filter(v, is_even);
	vect_basic_print(w);
	return 0;
}
