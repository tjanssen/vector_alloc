#include "../matrix-utils/matr_t-utils.h"
#include "../matr_t-core.h"
#include "../vector-utils/vect_t-utils.h"
#include <stddef.h>
#include <stdio.h>

#define SIZE_X 4
#define SIZE_Y 3

int main() {
	uint64_t size[] = {SIZE_X,SIZE_Y};
	matr_t *m1 = matr_alloc(2, size);
	int value = 0;
	for (int y=0; y<SIZE_Y; y++) {
		for (int x=0; x<SIZE_X; x++) {
			matr_inc_rc(m1, 1);
			uint64_t index[] = {x, y};
			matr_set(m1, index, value);
			value++;
		}
	}
	uint64_t vect_offset[] = {1,2};
	vect_t *v1 = matr_get_vect(m1, vect_offset, 2);
	vect_basic_print(v1);
	return 0;
}
