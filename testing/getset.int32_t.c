#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <stddef.h>
#include <stdio.h>

int main() {
	vect_t *v1 = vect_alloc(1);
	inc_rc(v1, 1);
	vect_set(v1, 0, 1);
	printf("%d\n", vect_get(v1, 0));
	return 0;
}
