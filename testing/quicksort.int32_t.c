#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"

int main(int argc, char *argv[]) {
  vect_t *v = vect_alloc(23);
  for (int i=0; i<23; i++) {
    inc_rc(v, 1);
    vect_set(v, i, (i*3)%23);
  }
  
  v = quick_sort(v);
  vect_basic_print(v);
  
  return 0;
}
