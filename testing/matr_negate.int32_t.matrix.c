#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"
#include <stdint.h>

VECT_DATATYPE counter_lmao = -1;

VECT_DATATYPE epic_setter(VECT_DATATYPE input[], uint32_t argc) {
	counter_lmao++;
	return counter_lmao;
}

int main() {
	uint64_t source_matrix_size[] = {5,5};
	matr_t *source_matrix = matr_alloc(2, source_matrix_size);
	source_matrix = matr_map(epic_setter, &source_matrix, 1);
	
	matr_t *b = matr_negate(source_matrix);
	matr_basic_print(b);
	return 0;
}
