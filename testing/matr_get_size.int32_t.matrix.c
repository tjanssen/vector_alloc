#include "../matrix-utils/matr_t-utils.h"
#include "../matr_t-core.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE_X 2
#define SIZE_Y 3

int main() {
	uint64_t size[] = {SIZE_X,SIZE_Y};
	matr_t *m1 = matr_alloc(2, size);
	int value = 0;
	uint64_t *size_pred = matr_get_size(m1);
	printf("%ld\n%ld\n", size_pred[0], size_pred[1]);
	free(size_pred);
	return 0;
}
