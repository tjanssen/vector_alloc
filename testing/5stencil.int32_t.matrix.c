#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"

#define SIZE_X 5
#define SIZE_Y 5

// this may overflow but that doesn't matter since this is a test anyway
VECT_DATATYPE counter_lmao = -1;

VECT_DATATYPE epic_setter(VECT_DATATYPE input[], uint32_t argc) {
	counter_lmao++;
	return counter_lmao;
}

int main() {
	uint64_t m_size[2] = {SIZE_X, SIZE_Y};
	matr_t *m = matr_alloc(2, m_size);
	m = matr_map(epic_setter, &m, 1);

	uint64_t result_size[2] = {SIZE_X-2, SIZE_Y-2};
	matr_t *result = matr_alloc(2, result_size);
	for (int i=1; i<SIZE_X-1; i++) {
		for (int j=1; j<SIZE_Y-1; j++) {
			VECT_DATATYPE accumulator = 0;
			uint64_t indices[2] = {i-1,j};
			accumulator += matr_get_nr(m, indices);
			indices[0] += 2;
			accumulator += matr_get_nr(m, indices);
			indices[0]--;
			accumulator += 4*matr_get_nr(m, indices);
			indices[1]++;
			accumulator += matr_get_nr(m, indices);
			indices[1] -= 2;
			accumulator += matr_get_nr(m, indices);
			indices[0]--;

			matr_set_nr(result, indices, accumulator);
		}
	}

	matr_dec_rc(m, 1);
	matr_basic_print(result);
}
