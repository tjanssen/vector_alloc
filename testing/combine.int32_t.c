#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"

#define VECT_SIZE 10

VECT_DATATYPE __product__(VECT_DATATYPE n1, VECT_DATATYPE n2) {
	return n1*n2;
}

int main() {
	vect_t *v1 = vect_alloc(VECT_SIZE);
	vect_t *v2 = vect_alloc(VECT_SIZE);
	for (int i=0; i<VECT_SIZE; i++) {
		inc_rc(v1, 1);
		inc_rc(v2, 1);
		vect_set(v1, i, i+1);
		vect_set(v2, i, i+1);
	}
	vect_t *result = vect_combine(__product__, v1, v2);
	vect_basic_print(result);
}
