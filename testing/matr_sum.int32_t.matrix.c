#include "../matrix-utils/matr_t-utils.h"
#include "../matr_t-core.h"
#include <stddef.h>
#include <stdio.h>

#define SIZE_X 2
#define SIZE_Y 3

int main() {
	uint64_t size[] = {SIZE_X,SIZE_Y};
	matr_t *m1 = matr_alloc(2, size);
	matr_t *m2 = matr_alloc(2, size);
	int value = 0;
	for (int x=0; x<SIZE_X; x++) {
		for (int y=0; y<SIZE_Y; y++) {
			matr_inc_rc(m1, 1);
			matr_inc_rc(m2, 1);
			uint64_t index[] = {x, y};
			matr_set(m1, index, value);
			matr_set(m2, index, value);
			value++;
		}
	}
	matr_t *m_result = matr_sum(m1, m2);
	matr_basic_print(m_result);
	return 0;
}
