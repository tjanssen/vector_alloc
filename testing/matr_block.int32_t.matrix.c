#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"
#include "../vector-utils/vect_t-utils.h"
#define SIZE_X 8
#define SIZE_Y 8
#define SIZE_Z 8
#define BLOCKSIZE 2

// this may overflow but that doesn't matter since this is a test anyway
VECT_DATATYPE counter_lmao = -1;

VECT_DATATYPE epic_setter(VECT_DATATYPE input[], uint32_t argc) {
	counter_lmao++;
	return counter_lmao;
}

int main() {
	uint64_t size0[] = {SIZE_X, SIZE_Y};
	uint64_t size1[] = {SIZE_Y, SIZE_Z};
	uint64_t size_r[] = {SIZE_X, SIZE_Z};
	
	matr_t *m0 = matr_alloc(2, size0);
	matr_t *m1 = matr_alloc(2, size1);
	m0 = matr_map(epic_setter, &m0, 1);
	m1 = matr_map(epic_setter, &m1, 1);
	matr_t *m_result = matr_block_multiply(m0, m1, BLOCKSIZE);
	
	matr_basic_print(m_result);
	return 0;
}
