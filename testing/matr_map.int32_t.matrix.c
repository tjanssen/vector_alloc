#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"

#define SIZE_X 3
#define SIZE_Y 4

VECT_DATATYPE test_sum(VECT_DATATYPE input[], uint32_t n) {
	uint32_t result = 0;
	for (int i=0; i<n; i++) {
		result += input[i];
	}
	return result;
}

int main() {
	uint64_t sizes[] = {SIZE_X, SIZE_Y};
	matr_t *matrices[2];
	matrices[0] = matr_alloc(2, sizes);
	matrices[1] = matr_alloc(2, sizes);
	uint64_t indices[] = {0,0};
	for (int i=0; i<SIZE_X*SIZE_Y-1; i++) {
		matr_inc_rc(matrices[0], 1);
		matr_inc_rc(matrices[1], 1);
		matr_set(matrices[0], indices, i);
		matr_set(matrices[1], indices, i);
		uint64_t d=0;
		while (indices[d] >= sizes[d]-1) {
			indices[d] = 0;
			d++;
		}
		indices[d]++;
	}
	matr_inc_rc(matrices[0], 1);
	matr_inc_rc(matrices[1], 1);
	matr_set(matrices[0], indices, SIZE_X*SIZE_Y-1);
	matr_set(matrices[1], indices, SIZE_X*SIZE_Y-1);
	
	matr_t *result = matr_map(test_sum, matrices, 2);
	matr_basic_print(result);
	return 0;
}
