# vector_alloc

vector_alloc aims to be a platform for array and matrix implementations. Different implementations can be easily compared and swapped out in complex applications that implement vector_alloc. End user applications need to perform explicit reference counting to prevent programs hogging memory. 

vector_alloc consists of four main parts; `core`, `utils` and `matr_t-core` and `matr_t-utils`. `core` can be (relatively) easily written and built by the programmer and swapped out. `utils` provides helper functions that do not need to be swapped out and that should work on any valid `core` implementation. Both variants with the `matr_t-` prefix work similarly, but provide implementations for matrices instead of vectors. 

## Build instructions

Use `make verify` to test functionality and `make benchmark` to perform benchmarks. Use `make benchmark-reference` to perform benchmarks on handwritten, optimized reference implementations. The test or benchmark set can be set in the Makefile or with a command-line make variable. 

## User settable make variables
| Variable | Description |
| --- | --- |
| `CFLAGS` | Flags to be used for compilation. |
| `CORE` | Vector core to be used. More cores can be entered separated by spaces. For each core with name `myCore` there should be a file `core/myCore.c`. |
| `CORE_SRC` | Can be used similar to `CORE`, but should be provided with full paths to core sourcefiles. `CORE` and `CORE_SRC` can not be used in conjunction. |
| `MATRIX_CORE` | Matrix core to be used. More cores can be entered separated by spaces. For each core with name `myCore` there should be a file `matr_t-core/myCore.c`. |
| `MATIRX_CORE_SRC` | Can be used similar to `MATRIX_CORE`, but should be provided with full paths to core sourcefiles. `MATRIX_CORE` and `MATRIX_CORE_SRC` can not be used in conjunction. |
| `TESTS` | Tests to be executed with the command `make verify`. Multiple tests can be passed separated by spaces. For each test with name `myTest.int32_t` there should be a file `testing/myTest.int32_t.c` and optionally a file `testing/myTest.int32_t.expected`. 
| `BENCHMARKS` | Benchmarks to be executed with the command `make benchmark`. Multiple benchmarks can be passed separated by spaces. For each benchmark with name `myBenchmark.int32_t` there should be a file `benchmark/myBenchmark.int32_t.c`. |
| `PASSES` | Number of times each benchmark should be run. |

## Using a custom corefile

None of the specifications listed in this section are final. 

### Reference counter usage

Functions that take vectors or matrices as input, consume this variable by decreasing its reference counter by 1. This does not include functions that end with `_nr`, which do not change the reference counter of their arguments. For functions with a `_nr` suffix, a reference counting counterpart is provided in `utils` or `matrix-utils`. 

The exact value of the reference counter is up to the programmer of the corefile. It does not need to reflect the actual number of references and it cannot be used as a heuristic outside of the corefile. 

### Required functions for vector corefiles

| Function prototype | Description |
| --- | --- |
| `void inc_rc (vect_t *v, uint16_t amount)` | Increase the reference counter of `v` by `amount`. |
| `void dec_rc(vect_t *v, uint16_t amount)` | Decrease the reference counter of `v` by `amount` and deallocate `v` is the reference counter would be 0 after calling this function. |
| `VECT_DATATYPE vect_get_nr(vect_t *v, uint32_t index)` | From `v`, get the element with index `index`. |
| `void vect_set_nr(vect_t *v, uint32_t index, VECT_DATATYPE elt)` | From `v`, set the element with index `index` to `elt`. |
| `uint32_t vect_get_size_nr(vect_t *v)` | Get the size of `v`. |
| `vect_t *vect_alloc(uint32_t size)` | Allocate an empty vector with size `size`. |
| `vect_t *vect_select_nr(vect_t *v, uint32_t offset, uint32_t size)` | Select a subvector of `v`, starting at `offset` and having length `size`. |
| `vect_t *vect_insert(vect_t *v1, vect_t *v2, uint32_t offset)` | Insert `v1` into `v2` such that the first element of `v1` has index `offset`. |

The type `VECT_DATATYPE` is defined in `vect_t-core.h`. 

### Required functions for matrix corefiles

| Function prototype | Description |
| --- | --- |
| `void matr_inc_rc(matr_t *m, uint16_t amount) ;` | Increase a matrix's reference counter.<br>`m` Matrix of which the reference counter is to be increased.<br>`amount` Amount by which the reference counter is to be increased. |
| `void matr_dec_rc(matr_t *m, uint16_t amount) ;` | Decrease a matrix's reference counter.<br>`m` Matrix of which the reference counter is to be decreased.<br>`amount` Amount by which the reference counter is to be decreased. |
| `VECT_DATATYPE matr_get_nr(matr_t *m, uint32_t index[]) ;` | Get an element from a matrix.<br>`m` Matrix from which the element should be retrieved.<br>`index` Array that denotes the index of the element. index should have the same length as the dimensionality of m. |
| `void matr_set_nr(matr_t *m, uint32_t index[], VECT_DATATYPE elt) ;` | Set an element in a matrix.<br>`m` Matrix in which the element should be set.<br>`index` Array that denotes the index of the element. index should have the same length as the dimensionality of `m`.<br>`elt` New value of the element. |
| `uint32_t matr_get_dim_nr(matr_t *m) ;` | Get the dimensionality of a matrix.<br>`m` Matrix from which the dimensionality should be retrieved. | 
| `uint32_t *matr_get_size_nr(matr_t *m) ;` | Get the size of a matrix.<br>`m` Matrix from which the size should be retrieved. |
| `matr_t *matr_alloc(uint32_t dimensionality, uint32_t size[]) ;` | Allocate a matrix<br>`dimensionality` Dimensionality of the matrix.<br>`size` Array containing the size of the new matrix. This array should have length `dimensionality`. |
| `matr_t *matr_select_nr(matr_t *source, uint32_t offset[], uint32_t size[]) ;` | Select a submatrix.<br>`source` Matrix from which the submatrix should be selected.<br>`offset` Array containing the offsets of the selection. This array should have length equal to the dimensionality of `source`, even if any of the elements is 0.<br>`size` Array denoting the size of the submatrix. This array should have length equal to the dimensionality of `source`, even if a dimension has size 1. |
| `matr_t *matr_transpose(matr_t *m);` | Transpose a matrix.<br>`m` Matrix that is to be transposed. |

## Testing

Custom corefiles can be verified using `make verify CORE=<name of core>`. Tests can be used individually by using `make verify TESTS='<test1> <test2> <test3>' CORE=<name of core>`.

More tests can be added by moving them to the `testing` folder. Use `#define VECT_DATATYPE my_type` to define the type used inside the vector. The source file of the test should be named `<testname>.<my_type>.c`. Expected program output can be put in `testing/<test>.expected`. If this file does not exist, the test will succeed as long as the program does not crash and there are no memory leaks. For instance, to execute the tests `rc` with datatype `int32_t` and `getset` with type `double`, use `make verify TESTS='rc.int32_t getset.double`. 

Please refer to the source files for tests to see what they test.

## Benchmarking

Built-in functions can be benchmarked using `make benchmark CORE='<core1> <core2>' BENCHMARKS=<bench1> <bench2>`.

Benchmarks work quite similar to tests, but aren't ran through valgrind and have some additional requirements. 

A benchmark should output the number of floating point operations per second in GFLOPS to `stdout`. For instance, a multiplication benchmark will look something like this: 

```c
#include "../utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <time.h>
#include <stdio.h>

// vector type to be used
#define VECT_DATATYPE int32_t

int main() {
	// preparations
	
	clock_t t0, t1;
	t0 = clock();
	
	// section that should be benchmarked
	
	t1 = clock();
	
	// cleanup
	printf("%f\n", get_gflops_for_matmul(m, n, p, t0, t1));
	return 0;
}
```

The function `get_gflops_for_matmul` is provided in `benchmark/benchmark-timing.c`. 

### Reference implementations

To test the performance of the modularity, there are some handwritten, optimized programs for matrix multiplication in the `reference` folder. These can be benchmarked using `make benchmark-reference BENCHMARK_REF='reference.<bench1> reference.<bench2>'`. 
