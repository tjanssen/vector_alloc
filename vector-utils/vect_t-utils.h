#include <stdbool.h>
#include "../vect_t-core.h"

#if VECT_DATATYPE == double
#define VECT_DATATYPE_FORMAT "%f"
#else
#define VECT_DATATYPE_FORMAT "%i"
#endif

/**
 * Filters out elements from a vector that fulfill a certain property. 
 * @param source Vector from which elements should be considered. 
 * @param func Function that decides whether elements should be filtered out. If this function returns a nonzero value for an element, this element is filtered out. 
 * @return The resulting vector. 
 */
vect_t *vect_filter(vect_t *source, char func(int)) ;

/**
 * Fold a vector into a single element. 
 * @param func Function that combines two elements into a single element. 
 * @param iv Initial value. 
 * @param v Vector that should be folded. 
 * @return The resulting value. 
 */
VECT_DATATYPE vect_foldr(VECT_DATATYPE func(VECT_DATATYPE, VECT_DATATYPE), VECT_DATATYPE iv, vect_t *v) ;

/**
 * Combine two vectors into a single vector. 
 * This function is similar to the zip function used in Haskell
 * @param func Function that combines two elements into a single element. 
 * @param v1 First vector from which elements should be taken. 
 * @param v2 Second vector from which elements should be taken. 
 * @return Resulting vector. 
 */
vect_t *vect_combine(VECT_DATATYPE func(VECT_DATATYPE, VECT_DATATYPE), vect_t *v1, vect_t *v2) ;

/**
 * Combine two vectors and then fold them. Similar to calling vect_combine and vect_foldr, but a lot faster
 * @param fold_func Function that is used for folding.
 * @param combine_func Function that is used for combining.
 * @param v1 First vector.
 * @param v2 Second vector.
 * @param iv Initial value used for folding.
 * @return The resulting value.
 */
VECT_DATATYPE vect_combine_and_fold(VECT_DATATYPE fold_func(VECT_DATATYPE, VECT_DATATYPE), VECT_DATATYPE combine_func(VECT_DATATYPE, VECT_DATATYPE), vect_t *v1, vect_t *v2, VECT_DATATYPE iv) ;

/**
 * Check if all elements from two vectors are equal. 
 * @return true v0 and v1 have the same length and all elements are the same, false otherwise. 
 */
bool vect_equal(vect_t *v0, vect_t *v1) ;

/**
 * Print a vector in a human-readable layout. 
 * @param source Vector that is to be printed. 
 */
void vect_pretty_print(vect_t *source) ;

/**
 * Print a vector with elements separated by newlines for debugging purposes. 
 * @param source Vector that is to be printed. 
 */
void vect_basic_print(vect_t *source) ;

/**
 * Checks whether a vector is sorted. 
 * @param input Input vector. 
 * @return true if input is sorted, false otherwise. 
 */
bool is_sorted(vect_t *input) ;

/**
 * Add one element to the end of a vector. 
 * @param input_vector Input vector. 
 * @param new_element Element that should be added. 
 * @return The resulting vector. 
 */
vect_t *append(vect_t *input_vector, VECT_DATATYPE new_element) ;

/**
 * Perform quicksort on a vector. 
 * @param input Input vector. 
 * @return The sorted vector. 
 */
vect_t *quick_sort(vect_t *input);

/*
 * Reference counting counterparts of core functions
 */

/**
 * Get an element from a vector. 
 * @param v Vector from which the element should be retrieved. 
 * @param index Index of the element. 
 * @return The element. 
 */
VECT_DATATYPE vect_get(vect_t *v, uint32_t index) ;

/**
 * Set an element in a vector. 
 * @param v Vector from which the element should be set. 
 * @param index Index of the element. 
 * @param elt New value of the element. 
 */
void vect_set(vect_t *v, uint32_t index, VECT_DATATYPE elt) ;

/**
 * Get the size of a vector. 
 * @param v Vector from which the size should be retrieved. 
 * @return Size of the vector. 
 */
uint32_t vect_get_size(vect_t *source) ;

/**
 * Select a subvector. 
 * @param v Vector from which the subvector should be selected. 
 * @param offset Offset of the selection. 
 * @param size Size of the selection
 * @return The selected subvector. 
 */
vect_t *vect_select(vect_t *source, uint32_t offset, uint32_t size) ;
