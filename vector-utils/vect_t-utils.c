#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "../vect_t-core.h"
#include "vect_t-utils.h"

vect_t *vect_filter(vect_t *source, char func(int)) {
  int new_index = 0;
  inc_rc(source, 1);
  for (int i=0; i<vect_get_size(source); i++) {
    inc_rc(source, 2);
    VECT_DATATYPE candidate = vect_get(source, i);
    if (func(candidate)) {
      inc_rc(source, 1);
      vect_set(source, new_index, candidate);
      new_index++;
    }
  }
  source = vect_select(source, 0, new_index);
  return source;
}

VECT_DATATYPE vect_foldr(VECT_DATATYPE func(VECT_DATATYPE, VECT_DATATYPE), VECT_DATATYPE iv, vect_t *v) {
  inc_rc(v, 1);
  uint32_t v_size = vect_get_size(v);
  for (uint32_t i=0; i<v_size; i++) {
    inc_rc(v, 1);
    iv=func(iv, vect_get(v, i));
  }
  dec_rc(v, 1);
  return iv;
}

vect_t *vect_combine(VECT_DATATYPE func(VECT_DATATYPE, VECT_DATATYPE), vect_t *v1, vect_t *v2) {
  inc_rc(v1, 1);
  inc_rc(v2, 1);
  uint32_t v1_size = vect_get_size(v1);
  uint32_t v2_size = vect_get_size(v2);
  if (v1_size != v2_size) {
    printf("Input vectors aren't of the same size!\n");
    dec_rc(v1, 1);
    dec_rc(v2, 1);
    return NULL;
  }
  vect_t *result = vect_alloc(v1_size);
  for (int i=0; i<v1_size; i++) {
    inc_rc(v1, 1);
    inc_rc(v2, 1);
    inc_rc(result, 1);
    vect_set(result, i, func(vect_get(v1, i), vect_get(v2, i)));
  }
  dec_rc(v1, 1);
  dec_rc(v2, 1);
  return result;
}

VECT_DATATYPE vect_combine_and_fold(VECT_DATATYPE fold_func(VECT_DATATYPE, VECT_DATATYPE), VECT_DATATYPE combine_func(VECT_DATATYPE, VECT_DATATYPE), vect_t *v1, vect_t *v2, VECT_DATATYPE iv) {
	inc_rc(v1, 1);
	inc_rc(v2, 1);
	uint32_t v1_size = vect_get_size(v1);
  uint32_t v2_size = vect_get_size(v2);
	if (v1_size < v2_size) v1_size = v2_size;

	for (int i=0; i<v1_size-1; i++) {
		iv = fold_func(iv, combine_func(vect_get_nr(v1, i), vect_get_nr(v2, i)));
	}

	iv = fold_func(iv, combine_func(vect_get(v1, v1_size-1), vect_get(v2, v1_size-1)));
	return iv;
}

// TODO: Add reference counting
// bool vect_equal(vect_t *v0, vect_t *v1) {
//   if (vect_get_size(v0) > vect_get_size(v1)) {
//     return false;
//   }
//   if (vect_get_size(v0) < vect_get_size(v1)) {
//     return false;
//   }
//   uint32_t compare_size = vect_get_size(v0);
//   uint32_t indices[] = {0,0};
//   for (uint32_t counter = 0; counter<compare_size; counter++) {
//     if (*vect_get_addr(v0, indices[0]) != *vect_get_addr(v1, indices[1])) return false;
//   }
//   return true;
// }

bool is_sorted(vect_t *input) {
  // here size was used instead of total_size
  if (vect_get_size(input) <= 1) return 1;
  VECT_DATATYPE number_left = vect_get(input, 0);
  VECT_DATATYPE number_right = vect_get(input, 1);
  if (number_left <= number_right) {
    vect_t *next = vect_select(input, 1, vect_get_size(input));
    bool return_value = is_sorted(next);
    dec_rc(next, 1);
    return return_value;
  }
  return 0;
}

void vect_pretty_print(vect_t *source) {
  inc_rc(source, 1);
  uint32_t elements_left = vect_get_size(source);
  printf("Size:\t%d\n", elements_left);
  if (elements_left != 0) {
    printf("Data:\t[");
    uint32_t index = 0;
    for (elements_left; elements_left > 0; elements_left--) {
      inc_rc(source, 1);
      printf("%d, ", vect_get(source, index));
      index++;
    }
    printf("]\n");
  }
  dec_rc(source, 1);
}

void vect_basic_print(vect_t *source) {
  inc_rc(source, 1);
  uint32_t source_size = vect_get_size(source);
  for (uint32_t index = 0; index < source_size; index++) {
    inc_rc(source, 1);
    printf("%d\n", vect_get(source, index));
  }
  dec_rc(source, 1);
}

vect_t *append(vect_t *input_vector, VECT_DATATYPE new_element) {
  vect_t *new_part = vect_alloc(1);
  inc_rc(new_part, 1);
  vect_set(new_part, 0, new_element);
  return vect_insert(input_vector, new_part, 0);
}
