#include <stddef.h>
#include "../vect_t-core.h"
#include "vect_t-utils.h"

vect_t *quick_sort(vect_t *input) {
  inc_rc(input, 1);
  uint32_t input_size = vect_get_size(input);
  if (input_size <= 1) {
    return input;
  }
  
  vect_t *left = vect_alloc(0);
  vect_t *middle = vect_alloc(1);
  vect_t *right = vect_alloc(0);
  
  inc_rc(input, 1);
  inc_rc(middle, 1);
  VECT_DATATYPE pivot = vect_get(input, 0);
  vect_set(middle, 0, pivot);
  char empty[2] = {1,1};
  for (int i=1; i<input_size; i++) {
    inc_rc(input, 1);
    VECT_DATATYPE candidate = vect_get(input, i);
    if (candidate < pivot) {
      left = append(left, candidate);
      empty[0] = 0;
    }
    else if (candidate > pivot) {
      right = append(right, candidate);
      empty[1] = 0;
    }
    else {
      middle = append(middle, candidate);
    }
  }
  
  dec_rc(input, 1);
  left = quick_sort(left);
  right = quick_sort(right);
  vect_t *intermediate_result = vect_insert(left, middle, 0);
  vect_t *result = vect_insert(intermediate_result, right, 0);
  return result;
}
