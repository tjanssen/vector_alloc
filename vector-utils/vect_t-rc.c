#include "../vect_t-core.h"

VECT_DATATYPE vect_get(vect_t *v, uint32_t index) {
  VECT_DATATYPE result = vect_get_nr(v, index);
  dec_rc(v, 1);
  return result;
}

void vect_set(vect_t *v, uint32_t index, VECT_DATATYPE elt) {
  vect_set_nr(v, index, elt);
  dec_rc(v, 1);
}

uint32_t vect_get_size(vect_t *source) {
  uint32_t result = vect_get_size_nr(source);
  dec_rc(source, 1);
  return result;
}

vect_t *vect_select(vect_t *source, uint32_t offset, uint32_t size) {
  vect_t *result = vect_select_nr(source, offset, size);
  dec_rc(source, 1);
  return result;
}
