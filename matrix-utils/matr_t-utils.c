#include "../matr_t-core.h"
#include "matr_t-utils.h"
#include "../vect_t-core.h"
#include "../vector-utils/vect_t-utils.h"
#include <stdio.h>
#include <stdlib.h>

// no safeguards cuz we livin on the edge
matr_t *matr_copy(matr_t *dest, matr_t *source, uint64_t offset[]) {
	uint64_t dimensionality = matr_get_dim_nr(source);
	uint64_t *copy_size = matr_get_size_nr(source);
	uint64_t indices[dimensionality];
	uint64_t result_indices[dimensionality];
	
	for (int i=0; i<dimensionality; i++) {
		indices[i] = 0;
		result_indices[i] = offset[i];
	}
	uint64_t *result_size = matr_get_size_nr(dest);
	matr_t *result = matr_select_nr(dest, indices, result_size);
	free(result_size);
	while (1) {
		matr_set_nr(result, result_indices, matr_get_nr(source, indices));
		
		int i=0;
		indices[i]++;
		result_indices[i]++;
		while (indices[i] >= copy_size[i]) {
			indices[i] = 0;
			result_indices[i] = offset[i];
			i++;
			if (i >= dimensionality) goto end;
			
			indices[i]++;
			result_indices[i]++;
		}
	}
	end:
	matr_dec_rc(dest, 1);
	matr_dec_rc(source, 1);
	free(copy_size);
	return result;
}

matr_t *matr_map(VECT_DATATYPE func(VECT_DATATYPE[],uint32_t), matr_t *input_matrices[], uint32_t argc) {
	uint64_t matr0_dim;
	if (argc > 0) {
		matr_inc_rc(input_matrices[0], 1);
		matr0_dim = matr_get_dim(input_matrices[0]);
	}
	for (int i=1; i<argc; i++) {
		matr_inc_rc(input_matrices[i], 1);
		if (matr_get_dim(input_matrices[i]) != matr0_dim) {
			printf("The dimensionality of the %dth matrix is unequal to the length of the first matrix. \n", i);
		}
	}
	uint64_t indices[matr0_dim];
	matr_inc_rc(input_matrices[0], 1);
	uint64_t *matrix_size = matr_get_size(input_matrices[0]);
	uint64_t vector_size = 1;
	for (int i=0; i<matr0_dim; i++) {
		indices[i] = 0;
		vector_size *= matrix_size[i];
	}
	matr_t *result_matrix = matr_alloc(matr0_dim, matrix_size);
	for (int i=0; i<vector_size-1; i++) {
		VECT_DATATYPE input_elements[argc];
		for (int m=0; m<argc; m++) {
			input_elements[m] = matr_get_nr(input_matrices[m], indices);
		}
		matr_set_nr(result_matrix, indices, func(input_elements, argc));
		uint64_t d = 0;
		while (indices[d] >= matrix_size[d]-1) {
			indices[d] = 0;
			d++;
		}
		indices[d]++;
	}
	VECT_DATATYPE input_elements[argc];
	for (int m=0; m<argc; m++) {
		input_elements[m] = matr_get(input_matrices[m], indices);
	}
	matr_inc_rc(result_matrix, 1);
	matr_set(result_matrix, indices, func(input_elements, argc));
	free(matrix_size);
	return result_matrix;
}

matr_t *matr_sum(matr_t *m1, matr_t *m2) {
	matr_inc_rc(m1, 2);
	uint64_t dimensionality = matr_get_dim(m1);
	uint64_t indices[dimensionality];
	uint64_t *result_size = matr_get_size(m1);
	uint64_t vector_size = 1;
	matr_t *result = matr_alloc(dimensionality, result_size);
	for (int i=0; i<dimensionality; i++) {
		indices[i] = 0;
		vector_size *= result_size[i];
	}
	for (int i=0; i<vector_size-1; i++) {
		matr_inc_rc(m1, 1);
		matr_inc_rc(m2, 1);
		matr_inc_rc(result, 1);
		matr_set(result, indices, matr_get(m1, indices)+matr_get(m2, indices));
		uint64_t d = dimensionality-1;
		while (indices[d] >= result_size[d]-1) {
			indices[d] = 0;
			d--;
		}
		indices[d]++;
	}
	matr_inc_rc(result, 1);
	matr_set(result, indices, matr_get(m1, indices)+matr_get(m2, indices));

	free(result_size);
	return result;
}

void matr_basic_print(matr_t *m) {
	matr_inc_rc(m, 2);
	uint64_t dimensionality = matr_get_dim(m);
	uint64_t indices[dimensionality];
	uint64_t *matrix_size = matr_get_size(m);
	uint64_t vector_size = 1;
	for (int i=0; i<dimensionality; i++) {
		indices[i] = 0;
		vector_size *= matrix_size[i];
	}
	for (int i=0; i<vector_size-1; i++) {
		matr_inc_rc(m, 1);
		VECT_DATATYPE thing = matr_get(m, indices);
		printf("%d\n", thing);
		// printf("%d\n",thing);
		uint64_t d = 0;
		while (indices[d] >= matrix_size[d]-1) {
			indices[d] = 0;
			d++;
			printf("\n");
		}
		indices[d]++;
	}
	free(matrix_size);
	printf("%d\n", matr_get(m, indices));
}

matr_t *matr_negate(matr_t *m) {
	uint64_t dim = matr_get_dim_nr(m);
	uint64_t *size = matr_get_size_nr(m);
	matr_t *result = matr_alloc(dim, size);
	uint64_t indices[dim];
	for (int d=0; d<dim; d++) {
		indices[d] = 0;
	}
	while (1) {
		matr_set_nr(result, indices, -matr_get_nr(m, indices));
		indices[0]++;
		uint64_t d=0;
		while (indices[d] >= size[d]) {
			indices[d] = 0;
			d++;
			if (d == dim) goto end;
			indices[d]++;
		}
	}
	end:
	free(size);
	matr_dec_rc(m, 1);
	return result;
}
