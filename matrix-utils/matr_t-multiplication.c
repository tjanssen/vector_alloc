#include "../vect_t-core.h"
#include "../vector-utils/vect_t-utils.h"
#include "../matr_t-core.h"
#include "matr_t-utils.h"
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <string.h>

VECT_DATATYPE __sum_array__(VECT_DATATYPE earray[], uint32_t argc) {
	VECT_DATATYPE result = 0;
	for (int i=0; i<argc; i++) {
		result+=earray[i];
	}
	return result;
}

// rework vect_combine_and_fold so that this function is not needed
VECT_DATATYPE __sum__(VECT_DATATYPE n1, VECT_DATATYPE n2) {
	return n1+n2;
}

VECT_DATATYPE __product__(VECT_DATATYPE n1, VECT_DATATYPE n2) {
	return n1*n2;
}

matr_t *matr_multiply(matr_t *m1, matr_t *m2) {
	matr_inc_rc(m1, 2);
	matr_inc_rc(m2, 2);
	uint64_t *m1_size = matr_get_size(m1);
	uint64_t *m2_size = matr_get_size(m2);
	uint64_t m1_dim = matr_get_dim(m1);
	uint64_t m2_dim = matr_get_dim(m2);
	if (m1_dim != 2 || m2_dim != 2) {
		// let's do multidimensional matrices later
		printf("Multiplication is only possible for twodimensional matrices.\n");
	}
	else if (m1_size[m1_dim-1] != m2_size[0]) {
		// do something more useful
		printf("Incompatible matrices\n");
	}
	
	uint64_t result_size[] = {m1_size[0], m2_size[1]};
	matr_t *m_result = matr_alloc(2, result_size);
	
	uint64_t indices[2] = {0,0};
	matr_t *m1_t = matr_transpose(m1);

	while (1) {
	// for the vertical offset the values are swapped since the matrix is transposed
	// before the vector is selected
		uint64_t vect_offset_h[2] = {0,indices[0]};
		uint64_t vect_offset_v[2] = {0,indices[1]};
		VECT_DATATYPE result_accumulator = 0;
		
		for (int i=0; i<m2_size[0]; i++) {
			result_accumulator += matr_get_nr(m1_t, vect_offset_h) * matr_get_nr(m2, vect_offset_v);
			vect_offset_h[0]++;
			vect_offset_v[0]++;
		}
		matr_set_nr(m_result, indices, result_accumulator);
		
		uint64_t d = 0;
		while (indices[d] >= result_size[d]-1) {
			indices[d] = 0;
			d++;
			if (d >= 2 /*m1->dimensionality+m2->dimensionality-1*/) {
				goto end;
			}
		}
		indices[d]++;
	}
	end:
	matr_dec_rc(m1_t, 1);
	matr_dec_rc(m2, 1);
	free(m1_size);
	free(m2_size);

	return m_result;
}

// strictly two-dimensional
void matr_block_multiply_dest(matr_t *m0, matr_t *m1, matr_t *m_result, uint64_t *result_offset, uint64_t blocksize) {
	matr_t *m1_t = matr_transpose(m1);
	matr_inc_rc(m0, 2);
	matr_inc_rc(m1_t, 2);
	uint64_t m0_dim = matr_get_dim(m0);
	uint64_t m1_dim = matr_get_dim(m1_t);
	uint64_t *m0_size = matr_get_size(m0);
	uint64_t *m1_size = matr_get_size(m1_t);
	if (m0_size[m0_dim-1] != m1_size[0]) {
		printf("Incompatible matrices\n");
	}
	if (m1_size[0] <= blocksize) {
		uint64_t result_coordinates[2] = {0,0};
		for (result_coordinates[1] = 0; result_coordinates[1] < m1_size[0]; result_coordinates[1]++) {
			for (result_coordinates[0] = 0; result_coordinates[0] < m0_size[0]; result_coordinates[0]++) {
				uint64_t real_result_coordinates[2] = {result_coordinates[0]+result_offset[0], result_coordinates[1]+result_offset[1]};
				VECT_DATATYPE accumulator = matr_get_nr(m_result, real_result_coordinates);
				for (uint64_t i = 0; i < m1_size[0]; i++) {
					uint64_t index_m0[2] = {result_coordinates[0], i};
					uint64_t index_m1[2] = {result_coordinates[1], i};
					accumulator += matr_get_nr(m0, index_m0) * matr_get_nr(m1_t, index_m1);
				}
				matr_set_nr(m_result, real_result_coordinates, accumulator);
			}
		}
	}
	else {
		uint64_t m0_sub00_size[m0_dim];
		m0_sub00_size[0] = m0_size[0] / 2 + m0_size[0] % 2;
		m0_sub00_size[1] = m0_size[1] / 2 + m0_size[1] % 2;
		uint64_t m0_sub01_size[m0_dim];
		m0_sub01_size[0] = m0_size[0] / 2 + m0_size[0] % 2;
		m0_sub01_size[1] = m0_size[1] / 2;
		uint64_t m0_sub10_size[m0_dim];
		m0_sub10_size[0] = m0_size[0] / 2;
		m0_sub10_size[1] = m0_size[1] / 2 + m0_size[1] % 2;
		uint64_t m0_sub11_size[m0_dim];
		m0_sub11_size[0] = m0_size[0] / 2;
		m0_sub11_size[1] = m0_size[1] / 2;
		uint64_t m0_sub00_offset[m0_dim];
		m0_sub00_offset[0] = 0;
		m0_sub00_offset[1] = 0;
		uint64_t m0_sub01_offset[m0_dim];
		m0_sub01_offset[0] = 0;
		m0_sub01_offset[1] = m0_size[1] / 2 + m0_size[1] % 2;
		uint64_t m0_sub10_offset[m0_dim];
		m0_sub10_offset[0] = m0_size[0] / 2 + m0_size[0] % 2;
		m0_sub10_offset[1] = 0;
		uint64_t m0_sub11_offset[m0_dim];
		m0_sub11_offset[0] = m0_size[0] / 2 + m0_size[0] % 2;
		m0_sub11_offset[1] = m0_size[1] / 2 + m0_size[1] % 2;
		
		uint64_t m1_sub00_size[m1_dim];
		m1_sub00_size[0] = m1_size[0] / 2 + m1_size[0] % 2;
		m1_sub00_size[1] = m1_size[1] / 2 + m1_size[1] % 2;
		uint64_t m1_sub01_size[m1_dim];
		m1_sub01_size[0] = m1_size[0] / 2 + m1_size[0] % 2;
		m1_sub01_size[1] = m1_size[1] / 2;
		uint64_t m1_sub10_size[m1_dim];
		m1_sub10_size[0] = m1_size[0] / 2;
		m1_sub10_size[1] = m1_size[1] / 2 + m1_size[1] % 2;
		uint64_t m1_sub11_size[m1_dim];
		m1_sub11_size[0] = m1_size[0] / 2;
		m1_sub11_size[1] = m1_size[1] / 2;
		uint64_t m1_sub00_offset[m1_dim];
		m1_sub00_offset[0] = 0;
		m1_sub00_offset[1] = 0;
		uint64_t m1_sub01_offset[m1_dim];
		m1_sub01_offset[0] = 0;
		m1_sub01_offset[1] = m1_size[1] / 2 + m1_size[1] % 2;
		uint64_t m1_sub10_offset[m1_dim];
		m1_sub10_offset[0] = m1_size[0] / 2 + m1_size[0] % 2;
		m1_sub10_offset[1] = 0;
		uint64_t m1_sub11_offset[m1_dim];
		m1_sub11_offset[0] = m1_size[0] / 2 + m1_size[0] % 2;
		m1_sub11_offset[1] = m1_size[1] / 2 + m1_size[1] % 2;
		
		uint64_t result_dim = m0_dim+m1_dim-2;
		uint64_t result_sub01_offset[2];
		result_sub01_offset[0] = result_offset[0];
		result_sub01_offset[1] = result_offset[1] + m1_size[1] / 2 + m1_size[1] % 2;
		uint64_t result_sub10_offset[2];
		result_sub10_offset[0] = result_offset[0] + m0_size[0] / 2 + m0_size[0] % 2;
		result_sub10_offset[1] = result_offset[1];
		uint64_t result_sub11_offset[2];
		result_sub11_offset[0] = result_offset[0] + m0_size[0] / 2 + m0_size[0] % 2;
		result_sub11_offset[1] = result_offset[1] + m1_size[1] / 2 + m1_size[1] % 2;
		
		matr_block_multiply_dest(matr_select_nr(m0, m0_sub00_offset, m0_sub00_size), 
														 matr_select_nr(m1_t, m1_sub00_offset, m1_sub00_size),
														 m_result, 
														 result_offset, 
														 blocksize);
		matr_block_multiply_dest(matr_select_nr(m0, m0_sub01_offset, m0_sub01_size), 
														 matr_select_nr(m1_t, m1_sub10_offset, m1_sub10_size),
														 m_result, 
														 result_offset, 
														 blocksize);
		
		matr_block_multiply_dest(matr_select_nr(m0, m0_sub00_offset, m0_sub00_size), 
														 matr_select_nr(m1_t, m1_sub01_offset, m1_sub01_size),
														 m_result, 
														 result_sub01_offset, 
														 blocksize);
		matr_block_multiply_dest(matr_select_nr(m0, m0_sub01_offset, m0_sub01_size), 
														 matr_select_nr(m1_t, m1_sub11_offset, m1_sub11_size),
														 m_result, 
														 result_sub01_offset, 
														 blocksize);
		
		matr_block_multiply_dest(matr_select_nr(m0, m0_sub10_offset, m0_sub10_size), 
														 matr_select_nr(m1_t, m1_sub00_offset, m1_sub00_size),
														 m_result, 
														 result_sub10_offset, 
														 blocksize);
		matr_block_multiply_dest(matr_select_nr(m0, m0_sub11_offset, m0_sub11_size), 
														 matr_select_nr(m1_t, m1_sub10_offset, m1_sub10_size),
														 m_result, 
														 result_sub10_offset, 
														 blocksize);
		
		matr_block_multiply_dest(matr_select_nr(m0, m0_sub10_offset, m0_sub10_size), 
														 matr_select_nr(m1_t, m1_sub01_offset, m1_sub01_size),
														 m_result, 
														 result_sub11_offset, 
														 blocksize);
		matr_block_multiply_dest(matr_select_nr(m0, m0_sub11_offset, m0_sub11_size), 
														 matr_select_nr(m1_t, m1_sub11_offset, m1_sub11_size),
														 m_result, 
														 result_sub11_offset, 
														 blocksize);
	}
	free(m0_size);
	free(m1_size);
	matr_dec_rc(m0, 1);
	matr_dec_rc(m1_t, 1);
}

matr_t *matr_block_multiply(matr_t *m0, matr_t *m1, uint64_t blocksize) {
	uint64_t m0_dim = matr_get_dim_nr(m0);
	uint64_t m1_dim = matr_get_dim_nr(m1);
	uint64_t *m0_size = matr_get_size_nr(m0);
	uint64_t *m1_size = matr_get_size_nr(m1);
	uint64_t result_size[m0_dim + m1_dim - 2];
	for (uint64_t i = 0; i < m0_dim-1; i++) {
		result_size[i] = m0_size[i];
	}
	for (uint64_t i = 0; i < m1_dim-1; i++) {
		result_size[i+m0_dim-1] = m1_size[i+1];
	}
	matr_t *m_result = matr_alloc(m0_dim+m1_dim-2, result_size);
	for (int y=0; y<result_size[1]; y++) {
		for (int x=0; x<result_size[0]; x++) {
			uint64_t index[2] = {x, y};
			matr_set_nr(m_result, index, 0);
		}
	}
	uint64_t offset[2] = {0,0};
	matr_block_multiply_dest(m0, m1, m_result, offset, blocksize);
	free(m0_size);
	free(m1_size);
	return m_result;
}

// specialized function for Strassen multiplication where the last element is negated
VECT_DATATYPE __sum_array_0__(VECT_DATATYPE earray[], uint32_t argc) {
	if (argc != 4) printf("argc isn't correct for __sum_mult_0__\n");
	return earray[0]+earray[1]+earray[2]-earray[3];
}

matr_t *matr_strassen(matr_t *a, matr_t *b, uint64_t cutoff) {
	uint64_t *size_a = matr_get_size_nr(a);
	if (size_a[0] <= cutoff) {
		free(size_a);
		return matr_multiply(a, b);
	}
	uint64_t size_half[2];
	size_half[0] = size_a[0]/2;
	size_half[1] = size_a[1]/2;
	uint64_t offset_zero[2] = {0,0};
	uint64_t offset_zero_half[2] = {0, size_half[1]};
	uint64_t offset_half_zero[2] = {size_half[0], 0};
	matr_inc_rc(a, 3);
	matr_inc_rc(b, 3);
	matr_t *a11 = matr_select(a, offset_zero, size_half);
	matr_t *a12 = matr_select(a, offset_zero_half, size_half);
	matr_t *a21 = matr_select(a, offset_half_zero, size_half);
	matr_t *a22 = matr_select(a, size_half, size_half);
	matr_t *b11 = matr_select(b, offset_zero, size_half);
	matr_t *b12 = matr_select(b, offset_zero_half, size_half);
	matr_t *b21 = matr_select(b, offset_half_zero, size_half);
	matr_t *b22 = matr_select(b, size_half, size_half);
	
	// intermediate matrices
	matr_inc_rc(a11, 3);
	matr_inc_rc(a12, 1);
	matr_inc_rc(a21, 1);
	matr_inc_rc(a22, 3);
	matr_inc_rc(b11, 3);
	matr_inc_rc(b12, 1);
	matr_inc_rc(b21, 1);
	matr_inc_rc(b22, 3);
	matr_t *m1 = matr_strassen(matr_sum(a11, a22), matr_sum(b11, b22), cutoff);
	matr_t *m2 = matr_strassen(matr_sum(a21, a22), b11, cutoff);
	matr_t *m3 = matr_strassen(a11, matr_sum(b12, matr_negate(b22)), cutoff);
	matr_t *m4 = matr_strassen(a22, matr_sum(b21, matr_negate(b11)), cutoff);
	matr_t *m5 = matr_strassen(matr_sum(a11, a12), b22, cutoff);
	matr_t *m6 = matr_strassen(matr_sum(a21, matr_negate(a11)), matr_sum(b11, b12), cutoff);
	matr_t *m7 = matr_strassen(matr_sum(a12, matr_negate(a22)), matr_sum(b21, b22), cutoff);
	
	// this is disgusting please remove this
	matr_inc_rc(m1, 1);
	matr_inc_rc(m2, 1);
	matr_inc_rc(m3, 1);
	matr_inc_rc(m4, 1);
	matr_inc_rc(m5, 1);
	
	matr_t *marray0[4] = {m1, m4, m7, m5};
	matr_t *marray1[4] = {m1, m3, m6, m2};
	
	matr_t *c11 = matr_map(__sum_array_0__, marray0, 4);
	matr_t *c12 = matr_sum(m3, m5);
	matr_t *c21 = matr_sum(m2, m4);
	matr_t *c22 = matr_map(__sum_array_0__, marray1, 4);
	
	matr_t *result = matr_alloc(2, size_a);
	result = matr_copy(result, c11, offset_zero);
	result = matr_copy(result, c12, offset_zero_half);
	result = matr_copy(result, c21, offset_half_zero);
	result = matr_copy(result, c22, size_half);
	free(size_a);
	return result;
}
