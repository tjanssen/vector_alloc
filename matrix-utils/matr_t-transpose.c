#include "../matr_t-core.h"
#include "matr_t-utils.h"
#include "../vect_t-core.h"
#include "../vector-utils/vect_t-utils.h"
#include <stdlib.h>

// this implementation is kinda expensive
matr_t *matr_transpose(matr_t *m) {
	uint64_t dimensionality = matr_get_dim_nr(m);
	uint64_t *size = matr_get_size_nr(m);
	uint64_t indices[dimensionality];
	uint64_t indices_reversed[dimensionality];
	uint64_t sizes_reversed[dimensionality];
	uint64_t vector_size = 1;
	for (int i=0; i<dimensionality; i++) {
		indices[i] = 0;
		indices_reversed[i] = 0;
		sizes_reversed[dimensionality-i-1] = size[i];

		// Use vectors for this if possible
		vector_size *= size[i];
	}
	matr_t *result = matr_alloc(dimensionality, sizes_reversed);
	for (int i=0; i<vector_size-1; i++) {
		matr_set_nr(result, indices_reversed, matr_get_nr(m, indices));
		uint64_t d = dimensionality-1;
		while (indices[d] >= size[d]-1) {
			indices[d] = 0;
			indices_reversed[dimensionality-d-1] = 0;
			d--;
		}
		indices[d]++;
		indices_reversed[dimensionality-d-1]++;
	}
	matr_set_nr(result, indices_reversed, matr_get_nr(m, indices));
	matr_dec_rc(m, 1);
	free(size);

	return result;
}
