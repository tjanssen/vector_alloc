#include "../matr_t-core.h"

/**
 * Take the element-wise sum of two matrices. 
 * The size of m1 should be equal to the size of m2. 
 * @param m1 First matrix. 
 * @param m2 Second matrix. 
 * @return The matrix obtained after taking the sum of m1 and m2. 
 */
matr_t *matr_sum(matr_t *m1, matr_t *m2) ;

/**
 * Multiply two matrices. 
 * As usual with matrix multiplication, the number of rows in m1 should be equal to the number of columns in m2. 
 * @param m1 First matrix. 
 * @param m2 Second matrix. 
 * @return The matrix obtained after multiplying m1 with m2. 
 */
matr_t *matr_multiply(matr_t *m1, matr_t *m2) ;

/**
 * Multiply two matrices in blocks for more efficient memory usage.
 * @param m1 First matrix. 
 * @param m2 Second matrix.
 * @param blocksize Size of the blocks. 
 * @return The matrix obtained after multiplying m1 with m2. 
 */
matr_t *matr_block_multiply(matr_t *m1, matr_t *m2, uint64_t blocksize) ;

/**
 * Multiply two matrices using Strassen's divide and conquer method. 
 * @param a First matrix.
 * @param b Second matrix
 * @param cutoff Cutoff for using the Strassen method instead of naive multiplication.
 * @return The matrix obtained after multiplying a with b. 
 */
matr_t *matr_strassen(matr_t *a, matr_t *b, uint64_t cutoff) ;

/**
 * Print a matrix with newlines separating each element and each row. 
 * Mostly used for debugging purposes. 
 * @param m The matrix that is to be printed. 
 */
void matr_basic_print(matr_t *m);

/**
 * Execute a function on each element in a matrix separately. 
 * Multiple matrices can be given as an argument in order to do more complex element-wise operation such as summation or scalar multiplication. This function cannot be used for more complex operations that need context from other elements, such as matrix multiplication. 
 * @param func Function that is to be executed on each element. 
 * @param input_matrices Array consisting of input matrices. 
 * @param argc Numer of arrays in input_matrices
 * @return The resulting matrix. 
 */
matr_t *matr_map(VECT_DATATYPE func(VECT_DATATYPE[], uint32_t), matr_t *input_matrices[], uint32_t argc) ;

/*
 * Reference counting counterparts of core functions
 */

/**
 * Get an element from a matrix
 * @param m Matrix from which the element should be retrieved. 
 * @param index Array that denotes the index of the element. index should have the same length as the dimensionality of m. 
 * @return The element. 
 */
VECT_DATATYPE matr_get(matr_t *m, uint64_t index[]) ;

/**
 * Set an element in a Matrix. 
 * @param m Matrix in which the element should be set. 
 * @param index Array that denotes the index of the element. index should have the same length as the dimensionality of m. 
 * @param elt New value of the element. 
 */
void matr_set(matr_t *m, uint64_t index[], VECT_DATATYPE elt) ;

/**
 * Get the dimensionality of a matrix. 
 * @param m Matrix from which the dimensionality should be retrieved. 
 * @return Dimensionality of the matrix. 
 */
uint64_t matr_get_dim(matr_t *m) ;

/**
 * Get the size of a matrix. 
 * @param m Matrix from which the size should be retrieved. 
 * @return Array with length dimensionality of m that denotes the size of m. 
 */
uint64_t *matr_get_size(matr_t *m) ;

/**
 * Select a submatrix. 
 * This function does not touch the reference counter of the matrix from which the submatrix is taken. 
 * @param source Matrix from which the submatrix should be selected. 
 * @param offset Array containing the offsets of the selection. This array should have length equal to the dimensionality of source, even if any of the elements is 0.  
 * @param size Array denoting the size of the submatrix. This array should have length equal to the dimensionality of source, even if a dimension has size 1. 
 * @return The selected submatrix. 
 */
matr_t *matr_select(matr_t *source, uint64_t offset[], uint64_t size[]) ;

/**
 * Copy a matrix into another matrix. 
 * @param dest Matrix in which should be copied. 
 * @param source Matrix to copy. 
 * @param offset Offset that the copied matrix should have. 
 */
matr_t *matr_copy(matr_t *dest, matr_t *source, uint64_t offset[]) ;

/**
 * Transpose a matrix.
 * @param m Matrix that is to be transposed.
 * @return The transposed matrix.
 */
matr_t *matr_transpose(matr_t *m);

/**
 * Negate a matrix
 * @param m Matrix that is to be negated. 
 * @return The negated matrix. 
 */
matr_t *matr_negate(matr_t *m);

matr_t *image_read(char *path) ;
void image_write(matr_t *image, char *path) ;
void image_pretty_print(matr_t *image) ;
void image_basic_print(matr_t *image) ;
matr_t *image_mask(matr_t *input_image, matr_t *mask, VECT_DATATYPE mask_scale_factor) ;
