#include "../matr_t-core.h"

VECT_DATATYPE matr_get(matr_t *m, uint64_t index[]) {
	VECT_DATATYPE result = matr_get_nr(m, index);
	matr_dec_rc(m, 1);
	return result;
}
void matr_set(matr_t *m, uint64_t index[], VECT_DATATYPE elt) {
	matr_set_nr(m, index, elt);
	matr_dec_rc(m, 1);
}
uint64_t *matr_get_size(matr_t *m) {
	uint64_t *result = matr_get_size_nr(m);
	matr_dec_rc(m, 1);
	return result;
}
uint64_t matr_get_dim(matr_t *m) {
	uint64_t result = matr_get_dim_nr(m);
	matr_dec_rc(m, 1);
	return result;
}
matr_t *matr_select(matr_t *source, uint64_t offset[], uint64_t size[]) {
	matr_t *result = matr_select_nr(source, offset, size);
	matr_dec_rc(source, 1);
	return result;
}

