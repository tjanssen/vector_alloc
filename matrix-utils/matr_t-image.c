#include "../matr_t-core.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

matr_t *image_read(char *path) {
	FILE *image = fopen(path, "r");
	if (image == NULL) {
		printf("Opening the file \"%s\" failed.\n", path);
		return 0;
	}
	char buf[64];
	fgets(buf, 64, image);
	if (!strcmp(buf, "P5")) {
		printf("Only the P5 image format is currently supported, continue at your own risk. \n");
	}
	do {
		buf[0] = fgetc(image);
	} while (buf[0] != '\n');
	uint64_t image_size[2];

	// This stops gdb's complaining about uninitialized variables.
	image_size[0] = 0;
	image_size[1] = 0;

	uint64_t image_white;
	fscanf(image, "%d %d\n", &image_size[0], &image_size[1]);
	fscanf(image, "%d\n", &image_white);
	// printf("x: %d, y: %d, white: %d\n", image_size[0], image_size[1], image_white);
	uint64_t image_bytesize = image_size[0]*image_size[1]*(image_white+1) >> 8;
	matr_t *result = matr_alloc(2, image_size);
	uint64_t index[2];
	for (index[0]=0; index[0]<image_size[0]; index[0]++) {
		for (index[1]=0; index[1]<image_size[1]; index[1]++) {
			int c = fgetc(image);
#if VECT_DATATYPE==float
			matr_set_nr(result, index, c*0.999999);
#else
			matr_set_nr(result, index, c);
#endif
		}
	}
	fclose(image);
	return result;
}
void image_write(matr_t *image, char *path) {
	FILE *image_file = fopen(path, "w");
	if (image_file == NULL) {
		printf("Opening the file \"%s\" failed.\n", path);
		return;
	}
	// headers
	fputs("P5\n", image_file);
	fputs("# Created by vector_alloc\n", image_file);
	uint64_t *image_size = matr_get_size_nr(image);

	fprintf(image_file, "%d %d\n", image_size[0], image_size[1]);
	// Change this if you want to use anything other than 8-bit color depth
	fputs("255\n", image_file);
	uint64_t image_index[2];
	for (image_index[0] = 0; image_index[0] < image_size[0]; image_index[0]++) {
		for (image_index[1] = 0; image_index[1] < image_size[1]; image_index[1]++) {
			char huts = (char)matr_get_nr(image, image_index);
			fputc(huts, image_file);
		}
	}
	matr_dec_rc(image, 1);
	free(image_size);
	fclose(image_file);
}
void image_pretty_print(matr_t *image) {
	uint64_t *image_size = matr_get_size_nr(image);
	uint64_t index[2];
	for (index[0] = 0; index[0]<image_size[0]; index[0]++) {
		for (index[1] = 0; index[1]<image_size[1]; index[1]++) {
			if (matr_get_nr(image, index) > 0) fputs("\x1b[47m  ", stdout);
			else fputs("\x1b[40m  ", stdout);
		}
		fputs("\x1b[0m\n", stdout);
	}
	free(image_size);
	matr_dec_rc(image, 1);
}

void image_basic_print(matr_t *image) {
	uint64_t *image_size = matr_get_size_nr(image);
	uint64_t index[2];
	for (index[0] = 0; index[0] < image_size[0]; index[0]++) {
		for (index[1] = 0; index[1] < image_size[1]; index[1]++) {
			printf("%d\n", matr_get_nr(image, index));
		}
	}
	free(image_size);
	matr_dec_rc(image, 1);
}

matr_t *image_mask(matr_t *input_image, matr_t *mask, VECT_DATATYPE mask_scale_factor) {
	uint64_t *mask_size = matr_get_size_nr(mask);
	uint64_t *image_size = matr_get_size_nr(input_image);
	uint64_t result_index[2];
	uint64_t padded_image_size[] = {image_size[0]+mask_size[0]-1, image_size[1]+mask_size[1]-1};
	uint64_t edge_size[] = {(mask_size[0]-1)/2, (mask_size[1]-1)/2};

	matr_t *padded_input_image = matr_alloc(2, padded_image_size);

	// copy matrix from og to padded matrix
	uint64_t padded_index[2];
	for (result_index[1] = 0, padded_index[1] = edge_size[1]; result_index[1] < image_size[1]; result_index[1]++, padded_index[1]++) {
		for (result_index[0] = 0, padded_index[0] = edge_size[0]; result_index[0] < image_size[0]; result_index[0]++, padded_index[0]++) {
			matr_set_nr(padded_input_image, padded_index, matr_get_nr(input_image, result_index));
		}
	}

	// mirror edges
	for (padded_index[1] = edge_size[1], result_index[1] = edge_size[1]; padded_index[1] < padded_image_size[1] - edge_size[1]; padded_index[1]++, result_index[1]++) {
		// top
		for (padded_index[0] = edge_size[0]+1, result_index[0] = edge_size[0]-1; result_index[0] < mask_size[0]; padded_index[0]++, result_index[0]--) {
			matr_set_nr(padded_input_image, result_index, matr_get_nr(padded_input_image, padded_index));
		}

		// bottom
		for (padded_index[0] = edge_size[0]+image_size[0]-2, result_index[0] = edge_size[0]+image_size[0]; padded_index[0] >= image_size[0]-1; padded_index[0]--, result_index[0]++) {
			matr_set_nr(padded_input_image, result_index, matr_get_nr(padded_input_image, padded_index));
		}
	}

	for (padded_index[0] = 0, result_index[0] = 0; padded_index[0] < padded_image_size[0]; padded_index[0]++, result_index[0]++) {
		// left
		for (padded_index[1] = edge_size[1]+1, result_index[1] = edge_size[1]-1; result_index[1] < mask_size[1]; padded_index[1]++, result_index[1]--) {
			matr_set_nr(padded_input_image, result_index, matr_get_nr(padded_input_image, padded_index));
		}

		// right
		for (padded_index[1] = edge_size[1]+image_size[1]-2, result_index[1] = edge_size[1]+image_size[1]; padded_index[1] >= image_size[1]-1; padded_index[1]--, result_index[1]++) {
			matr_set_nr(padded_input_image, result_index, matr_get_nr(padded_input_image, padded_index));
		}
	}

	matr_t *output_image = matr_alloc(2, image_size);

	for (result_index[1] = 0; result_index[1] < image_size[1]; result_index[1]++) {
		for (result_index[0] = 0; result_index[0] < image_size[0]; result_index[0]++) {
			VECT_DATATYPE cell_result = 0;
			uint64_t mask_index[2];
			for (mask_index[1] = 0, padded_index[1] = result_index[1]; mask_index[1] < mask_size[1]; mask_index[1]++, padded_index[1]++) {
				for (mask_index[0] = 0, padded_index[0] = result_index[0]; mask_index[0] < mask_size[0]; mask_index[0]++, padded_index[0]++) {
					cell_result += (matr_get_nr(padded_input_image, padded_index) * matr_get_nr(mask, mask_index))/mask_scale_factor;
				}
			}
			matr_set_nr(output_image, result_index, cell_result);
		}
	}
	matr_dec_rc(input_image, 1);
	matr_dec_rc(padded_input_image, 1);
	matr_dec_rc(mask, 1);
	free(image_size);
	free(mask_size);
	return output_image;
}
