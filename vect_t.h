#ifndef VECT_T_
#define VECT_T_


#include <stdbool.h>
#include <stdint.h>

enum mode{DYNAMIC, FORCE_COPY, FORCE_REFERENCE};

/**
 * vect_t is a structure that defines vectors
 * 
 * data: Pointer to the data contained in the struct. The allocation pointed to 
 * by _data_ should have at least size _size_. 
 * size: The total size of data contained in _data_. 
 * rc: This _vect_t_'s reference count
 * next: Pointer to the next _vect_t_. Can be NULL when there is no next data. 
 */





vect_t *vect_alloc(uint32_t vec_size) ;

vect_t *vect_realloc(vect_t *input, uint32_t new_size) ;





#endif
