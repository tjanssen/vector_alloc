#include <stdint.h>

typedef struct VECT_T vect_t; 

/**
 * Increase a vector's reference counter.
 * @param v Vector of which the reference counter is to be increased.
 * @param amount Amount by which the reference counter is to be increased. 
 */
void inc_rc(vect_t *v, uint16_t amount) ;

/**
 * Decrease a vector's reference counter.
 * @param v Vector of which the reference counter is to be decreased.
 * @param amount Amount by which the reference counter is to be decreased. 
 */
void dec_rc(vect_t *v, uint16_t amount) ;

/**
 * Get an element from a vector. 
 * @param v Vector from which the element should be retrieved. 
 * @param index Index of the element. 
 * @return The element. 
 */
VECT_DATATYPE vect_get_nr(vect_t *v, uint32_t index) ;

/**
 * Set an element in a vector. 
 * @param v Vector from which the element should be set. 
 * @param index Index of the element. 
 * @param elt New value of the element. 
 */
void vect_set_nr(vect_t *v, uint32_t index, VECT_DATATYPE elt) ;

/**
 * Get the size of a vector. 
 * @param v Vector from which the size should be retrieved. 
 * @return Size of the vector. 
 */
uint32_t vect_get_size_nr(vect_t *v) ;

/**
 * Allocate an empty vector. 
 * @param size Size of the vector that is to be allocated. 
 * @return The newly allocated vector. 
 */
vect_t *vect_alloc(uint32_t size);

/**
 * Select a subvector. 
 * @param v Vector from which the subvector should be selected. 
 * @param offset Offset of the selection. 
 * @param size Size of the selection
 * @return The selected subvector. 
 */
vect_t *vect_select_nr(vect_t *v, uint32_t offset, uint32_t size) ;

/**
 * Insert a vector into another vector. 
 * @param v1 Vector that is to be inserted. 
 * @param v2 Vector in which the vector is inserted. 
 * @param offset Offset in which the vector is inserted. 
 * @return The resulting vector. 
 */
vect_t *vect_insert(vect_t *v1, vect_t *v2, uint32_t offset) ;
