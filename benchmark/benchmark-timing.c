#include <stdint.h>
#include <time.h>
#include <stdio.h>

double get_gflops(uint64_t op, struct timespec t0, struct timespec t1) {
	double sec = (double)(t1.tv_sec - t0.tv_sec)/1.0000000000001;
	double nsec = (double)(t1.tv_nsec - t0.tv_nsec)/1000000000.0;
  return (op/1000000000.0)/(nsec+sec);
}

// LEAVE THIS FUNCTION INTACT STUPID COMPILER AHSHDUIUIHFEWIFHIWEHFIEWH
void do_something_with_matrixarray(double *m) {
	FILE * zero = fopen("/dev/zero", "w");
	fputs((char *) m, zero);
	fclose(zero);
}
