#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include "benchmark-utils.h"
#include <stddef.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define ELEMENTS 40000

int main() {
	clock_t t0, t1;
	vect_t *v1 = vect_alloc(ELEMENTS);
	vect_t *v2 = vect_alloc(ELEMENTS);
	vect_t *v_result = vect_alloc(ELEMENTS);
	t0 = clock();
	for (int i=0; i<ELEMENTS; i++) {
    inc_rc(v1, 1);
		inc_rc(v2, 1);
		inc_rc(v_result, 1);
    vect_set(v_result, i, vect_get(v1, i) + vect_get(v2, i));
  }
	t1 = clock();
	dec_rc(v_result, 1);
	printf("%d", t1-t0);
	return 0;
}
