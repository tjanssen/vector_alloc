#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"
#include "benchmark-timing.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define OPS_PER_VALUE 5

// The third argument is ignored. Blame the Makefile. 
int main(int argc, char *argv[]) {
	struct timespec t0, t1;
	if (argc != 4) {
		printf("Expected 3 arguments but was given %d. \n", argc-1);
		return 1;
	}
	uint64_t m_size[2] = {atoi(argv[1]), atoi(argv[2])};
	matr_t *m = matr_alloc(2, m_size);

	uint64_t result_size[2] = {m_size[0]-2, m_size[1]-2};
	matr_t *result = matr_alloc(2, result_size);
	clock_gettime(CLOCK_REALTIME, &t0);
	for (int i=1; i<m_size[0]-1; i++) {
		for (int j=1; j<m_size[1]-1; j++) {
			VECT_DATATYPE accumulator = 0;
			uint64_t indices[2] = {i-1,j};
			accumulator += matr_get_nr(m, indices);
			indices[0] += 2;
			accumulator += matr_get_nr(m, indices);
			indices[0]--;
			accumulator += 4*matr_get_nr(m, indices);
			indices[1]++;
			accumulator += matr_get_nr(m, indices);
			indices[1] -= 2;
			accumulator += matr_get_nr(m, indices);
			indices[0]--;

			matr_set_nr(result, indices, accumulator);
		}
	}

	matr_dec_rc(m, 1);
	clock_gettime(CLOCK_REALTIME, &t1);
	printf("%f\n", get_gflops(OPS_PER_VALUE*(m_size[0]-2)*(m_size[1]-2), t0, t1));
}
