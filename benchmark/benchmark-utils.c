#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include <stdlib.h>

vect_t *generate_vector_with_random_numbers(uint32_t size) {
  vect_t *newVector = vect_alloc(size);
  for (int i=0; i<size; i++) {
    inc_rc(newVector, 1);
    vect_set(newVector, i, rand()%(size*10));
  }
  return newVector;
}
