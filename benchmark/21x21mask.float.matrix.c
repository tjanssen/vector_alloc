#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"
#include "../vector-utils/vect_t-utils.h"
#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "benchmark-utils.h"
#include "benchmark-timing.h"

#define MASK_X 21
#define MASK_Y 21

int main(int argc, char *argv[]) {
	if (argc != 4) {
		printf("Expected 3 arguments but was given %d. \n", argc-1);
		return 1;
	}
	uint64_t m = atoi(argv[1]);
	uint64_t n = atoi(argv[2]);
	struct timespec t0, t1;
	uint64_t matrix_size[] = {m,n};
	uint64_t mask_size[] = {MASK_X, MASK_Y};

	matr_t *image = matr_alloc(2, matrix_size);
	matr_t *mask = matr_alloc(2, mask_size);

	clock_gettime(CLOCK_REALTIME, &t0);
	matr_t *m_result = image_mask(image, mask, 6.9);
	clock_gettime(CLOCK_REALTIME, &t1);

	printf("%f\n", get_gflops(m*n*(2*MASK_X*MASK_Y-1), t0, t1));
	matr_dec_rc(m_result, 1);
	return 0;
}
