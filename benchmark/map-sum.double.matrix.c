#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "benchmark-utils.h"

int main(int argc, char *argv[]) {
	if (argc != 3) {
		printf("Expected 2 arguments but was given %d. \n", argc-1);
		return 1;
	}
	uint64_t m = atoi(argv[1]);
	uint64_t n = atoi(argv[2]);
	uint64_t blocksize = 64;
	struct timespec t0, t1;
	uint32_t dimensions[2] = {m,n};
	matr_t *m1 = matr_alloc(2, dimensions);
	matr_t *m2 = matr_alloc(2, dimensions);
	
	clock_gettime(CLOCK_REALTIME, &t0);
	matr_t *m_result = matr_sum(m1, m2);
	clock_gettime(CLOCK_REALTIME, &t1);
	
	printf("%f\n", get_flops_for_matsum(m, n, t0, t1));
	matr_dec_rc(m_result, 1);
	return 0;
}
