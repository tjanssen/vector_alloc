#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include "benchmark-utils.h"
#include <stddef.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define SEGMENTS 1
#define SEGSIZE 10

int main() {
	vect_t *v0, *v1;
  v0 = vect_alloc(SEGSIZE);
	clock_t t0, t1;
	t0 = clock();
	v1 = vect_select(v0, 0, SEGSIZE);
	t1 = clock();
	dec_rc(v0, 1);
	dec_rc(v1, 1);
	printf("%d", t1-t0);
	return 0;
}
