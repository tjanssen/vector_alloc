#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include "benchmark-utils.h"
#include <stddef.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define SEGMENTS 1
#define SEGSIZE 10

int main() {
	vect_t *v;
	v = vect_alloc(SEGSIZE);
	clock_t t0, t1;
	t0 = clock();
  inc_rc(v, 1);
	t1 = clock();
	dec_rc(v, 2);
	printf("%d", t1-t0);
	return 0;
}
