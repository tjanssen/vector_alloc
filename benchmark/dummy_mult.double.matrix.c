#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"
#include "../vector-utils/vect_t-utils.h"
#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "benchmark-utils.h"
#include "benchmark-timing.h"

int main () {
	struct timespec t0, t1;
	clock_gettime(CLOCK_REALTIME, &t0);
	clock_gettime(CLOCK_REALTIME, &t1);
	t1.tv_sec++;

	printf("%f\n", get_gflops(2048*2048*(2*2048-1), t0, t1));
	return 0;
}
