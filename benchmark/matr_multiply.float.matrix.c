#include "../matr_t-core.h"
#include "../matrix-utils/matr_t-utils.h"
#include "../vector-utils/vect_t-utils.h"
#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "benchmark-utils.h"
#include "benchmark-timing.h"

VECT_DATATYPE counter_lmao = -1.0;

VECT_DATATYPE epic_setter(VECT_DATATYPE input[], uint32_t argc) {
	counter_lmao++;
	return counter_lmao;
}

int main(int argc, char *argv[]) {
	if (argc != 4) {
		printf("Expected 3 arguments but was given %d. \n", argc-1);
		return 1;
	}
	uint64_t m = atoi(argv[1]);
	uint64_t n = atoi(argv[2]);
	uint64_t p = atoi(argv[3]);
	struct timespec t0, t1;
	uint64_t matrix_size_0[] = {m,n};
	uint64_t matrix_size_1[] = {n,p};

	matr_t *m0 = matr_alloc(2, matrix_size_0);
	matr_t *m1 = matr_alloc(2, matrix_size_1);
	m0 = matr_map(epic_setter, &m0, 1);
	m1 = matr_map(epic_setter, &m1, 1);

	clock_gettime(CLOCK_REALTIME, &t0);
	matr_t *m_result = matr_multiply(m0, m1);
	clock_gettime(CLOCK_REALTIME, &t1);

	printf("%f\n", get_gflops_for_matmul(m, n, p, t0, t1));
	matr_dec_rc(m_result, 1);
	return 0;
}
