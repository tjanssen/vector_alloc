#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include "benchmark-utils.h"
#include <stddef.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define SEGMENTS 1
#define SEGSIZE 1

int main() {
	clock_t t0, t1;
	VECT_DATATYPE result;
	vect_t *v = generate_vector_with_random_numbers(SEGSIZE);
	for (int i=1; i<SEGMENTS; i++) {
		v = vect_insert(v, generate_vector_with_random_numbers(SEGSIZE), 0);
	}
	t0 = clock();
	result = vect_get(v, 0);
	t1 = clock();
	dec_rc(v, 1);
	printf("%d", t1-t0);
	return 0;
}
