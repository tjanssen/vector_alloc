#include "../vector-utils/vect_t-utils.h"
#include "../vect_t-core.h"
#include "benchmark-utils.h"
#include <stddef.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define SEGMENTS 2000
#define SEGSIZE 2
#define VECT_DATATYPE double

int main() {
	clock_t t0, t1;
	vect_t *v1 = generate_vector_with_random_numbers(SEGSIZE);
	vect_t *v2 = generate_vector_with_random_numbers(SEGSIZE);
	vect_t *v_result = vect_alloc(SEGMENTS*SEGSIZE);
	for (int i=1; i<SEGMENTS; i++) {
		v1 = vect_insert(v1, generate_vector_with_random_numbers(SEGSIZE), 0);
		v2 = vect_insert(v2, generate_vector_with_random_numbers(SEGSIZE), 0);
	}
	t0 = clock();
	for (int i=0; i<SEGMENTS*SEGSIZE; i++) {
    inc_rc(v1, 1);
		inc_rc(v2, 1);
		inc_rc(v_result, 1);
    vect_set(v_result, i, vect_get(v1, i) + vect_get(v2, i));
  }
	t1 = clock();
	dec_rc(v_result, 1);
	printf("%d", t1-t0);
	return 0;
}
