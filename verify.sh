COREFILE=$2
COREFILE_MATRIX=$3
for TEST in $1
do
  if [[ ${TEST##*.} != "matrix" ]]; then
    OUTPUT=`valgrind -q --leak-check=full --exit-on-first-error=yes --error-exitcode=1 --log-fd=1 bin/test-$COREFILE.$TEST`;
  else
    OUTPUT=`valgrind -q --leak-check=full --exit-on-first-error=yes --error-exitcode=1 --log-fd=1 bin/test-$COREFILE_MATRIX.$COREFILE.$TEST`;
  fi
  if [[ $? -eq 0 ]]; then
    if [[ -f "testing/$TEST.expected" ]]; then
      EXP_OUTPUT=`cat testing/$TEST.expected`;
      if [[ ${TEST##*.} != "matrix" ]]; then
        REAL_OUTPUT=`bin/test-$COREFILE.$TEST`;
      else
        REAL_OUTPUT=`bin/test-$COREFILE_MATRIX.$COREFILE.$TEST`;
      fi        
      if [[ $EXP_OUTPUT == $REAL_OUTPUT ]]; then
        if [[ $PASSED == "" ]]; then
          PASSED="$TEST";
        else
          PASSED="$PASSED, $TEST";
        fi
      else
        echo -n "Output does not match expected output in test <$TEST>.\\n\
Expected output:\\n$(echo "$EXP_OUTPUT" | sed '{:q;N;s/\n/\\n/g;t q}')\\n\
Real output:\\n$(echo "$REAL_OUTPUT" | sed '{:q;N;s/\n/\\n/g;t q}')\\n";
        if [[ $FAILED == "" ]]; then
          FAILED="$TEST";
        else
          FAILED="$FAILED, $TEST";
        fi
      fi
    else
      if [[ $PASSED == "" ]]; then
        PASSED="$TEST";
      else
        PASSED="$PASSED, $TEST";
      fi
    fi
  else
    if [[ $FAILED == "" ]]; then
      FAILED="$TEST";
    else
      FAILED="$FAILED, $TEST";
    fi
    echo -n "Errors found in test <$TEST>:\\n";
    OUTPUT=$(echo "$OUTPUT" | sed '{:q;N;s/\n/\\n/g;t q}');
    echo -n "$OUTPUT\\n";
  fi
done
echo "Tests passed: $PASSED\\nTests failed: $FAILED\\n";
